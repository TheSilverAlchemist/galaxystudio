#-------------------------------------------------
#
# Project created by QtCreator 2012-09-22T15:48:04
#
#-------------------------------------------------

exists(settings.pro):include(settings.pro)
else:message("I can't find settings file")

QT       += core gui opengl xml sql
config += share link_prl

TARGET = GalaxyStudio
TEMPLATE = app

INCLUDEPATH += ../include

SOURCES += \
    ../src/main.cpp \
    ../src/galaxyviewcontroller.cpp \
    ../src/galaxygenerator.cpp \
    ../src/star.cpp \
    ../src/galaxygeneratorconfigdialog.cpp \
    ../src/galaxycontainer.cpp \
    ../src/maincontroldialog.cpp \
    ../src/MovableText.cpp \
    ../src/sectorredefinedialog.cpp \
    ../src/galaxyview.cpp \
    ../src/sectorview.cpp \
    ../src/sectorviewdialog.cpp \
    ../src/systemview.cpp \
    ../src/systemviewdialog.cpp

HEADERS  += \
    ../include/galaxyviewcontroller.h \
    ../include/galaxygenerator.h \
    ../include/star.h \
    ../include/galaxygeneratorconfigdialog.h \
    ../include/galaxycontainer.h \
    ../include/maincontroldialog.h \
    ../include/Globals.h \
    ../include/MovableText.h \
    ../include/sectorredefinedialog.h \
    ../include/galaxyview.h \
    ../include/sectorview.h \
    ../include/sectorviewdialog.h \
    ../include/systemview.h \
    ../include/systemviewdialog.h

FORMS    += \
    ../ui/galaxygeneratorconfigdialog.ui \
    ../ui/maincontroldialog.ui \
    ../ui/sectorredefinedialog.ui \
    ../ui/sectorviewdialog.ui \
    ../ui/systemviewdialog.ui

CONFIG(debug, debug|release) {
  TARGET = $$join(TARGET,,,d)
  LIBS *= -lOgreMain -lQtOgre
}
CONFIG(release, debug|release) {
  LIBS *= -lOgreMain -lQtOgre
}

