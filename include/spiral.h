#ifndef SPIRAL_H
#define SPIRAL_H

class Spiral {

public:
    Spiral( double maxTurns, double maxRadius );
    virtual ~Spiral();

    virtual std::string getName();

    virtual double toRadius( double angle );
    virtual double toAngle( double radius );

    double getSpiralDistance( int x, int y );
    void setMaximumTurns( double maxTurns );
    double getMaximumTurns();
    double getMaximumRadius();

protected:
    double mParameter;
    double mMaximumTurns;
    double mMaximumRadius;

};



class ArchimedesSpiral : public Spiral {

public:
    ArchimedesSpiral( double maxTurns, double maxRadius );
    ~ArchimedesSpiral();

    std::string getName();
    double toRadius( double angle );
    double toAngle( double radius );

};


class FermatSpiral : public Spiral {

public:
    FermatSpiral( double maxTurns, double maxRadius );
    ~FermatSpiral();

    std::string getName();
    double toRadius( double angle );
    double toAngle( double radius );

};



class NoisySpiral : public Spiral {

public:
    NoisySpiral( double maxTurns, double maxRadius );
    ~NoisySpiral();

    std::string getName();
    double toRadius( double angle );
    double toAngle( double radius );

};


class HyperbolicSpiral : public Spiral {

public:
    HyperbolicSpiral( double maxTurns, double maxRadius );
    ~HyperbolicSpiral();

    std::string getName();
    double toRadius( double angle );
    double toAngle( double radius );

};



class SpiralFactory {

public:
    SpiralFactory();
    ~SpiralFactory();

    static Spiral *CreateSpiralFunction( std::string spiralType, double maxTurns, double maxRadius );

};



#endif // SPIRAL_H
