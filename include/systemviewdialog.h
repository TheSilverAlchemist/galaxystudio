#ifndef SYSTEMVIEWDIALOG_H
#define SYSTEMVIEWDIALOG_H

#include <QDialog>

namespace Ui {
class SystemViewDialog;
}

class SystemView;

class SystemViewDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SystemViewDialog(SystemView *pView, QWidget *parent = 0);
    ~SystemViewDialog();
    
private slots:
    void on_randomSystemButton_pressed();

    void on_simTimeStep_valueChanged(double arg1);

private:
    Ui::SystemViewDialog *ui;
    SystemView *mSystemView;
};

#endif // SYSTEMVIEWDIALOG_H
