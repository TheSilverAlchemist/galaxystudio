#ifndef GALAXYVIEW_H
#define GALAXYVIEW_H


#include <OgrePrerequisites.h>
#include <QPoint>
#include <QHash>
#include <QEvent>
#include <QTime>

#include "Globals.h"
#include "galaxyviewcontroller.h"
#include "galaxycontainer.h"

class MainControlDialog;
class GalaxyGenerator;
class GalaxyGeneratorConfigDialog;
class GalaxyContainer;

class QKeyEvent;
class QMouseEvent;
class QWheelEvent;

namespace QtOgre{
    class GalaxyViewController;
};


class GalaxyView
{
public:
    GalaxyView(QtOgre::GalaxyViewController *);

    void initialise(void);
    void resume(void);
    void update(void);
    void halt(void);
    void shutdown(void);

    void onKeyPress(QKeyEvent* event);
    void onKeyRelease(QKeyEvent* event);

    void onMouseMove(QMouseEvent* event);
    void onMousePress(QMouseEvent* event);
    void onMouseRelease(QMouseEvent* event);
    void onWheel(QWheelEvent* event);

    void HighlightSector(int sectorId);
    void HighlightSector(int r, int t, int p);
    void HighlightSector( GalaxyContainer::SectorData *pSector );

    void doGalaxyCreate();
    void doGalaxyLoad();

    QtOgre::GalaxyViewController *GetViewController();

    void GoToHighlightedSector();

protected:

    QtOgre::GalaxyViewController *mViewController;

    int mLastFrameWheelPos;
    int mCurrentWheelPos;
    QTime* mTime;

    int mLastFrameTime;
    int mCurrentTime;

    bool mIsFirstFrame;

    float mCameraSpeed;

    enum InterfaceAction {
        MA_None = 0,
        MA_Select,
        MA_AstrolabeMove,
        MA_InclinometerMove,
        MA_Rotate,
        MA_Zoom
    };



    QHash<int, KeyStates> mKeyStates;
    QPoint mLastFrameMousePos;
    QPoint mCurrentMousePos;
    InterfaceAction mAction;

    Ogre::BillboardSet *mStarBillboardSet;
    Ogre::SceneNode *mGalaxySceneNode;

    Ogre::SceneNode *mHighlightSectorNode;
    int mHighlightedSector;

    GalaxyGeneratorConfigDialog *mGalaxyGen;
    MainControlDialog *mMainControl;

    Ogre::SceneNode *mAstrolabeSceneNode;
    void GenerateAstrolabe();
    Ogre::SceneNode *mInclinometerSceneNode;
    void GenerateInclinometer();

    Ogre::RaySceneQuery *mRayQuery;

    Ogre::SceneNode *mAstrolabeMarkerSceneNode;
    double mAstrolabeMark;
    Ogre::SceneNode *mInclinometerMarkerSceneNode;
    double mInclinometerMark;

    Ogre::Camera* mCamera;
    Ogre::SceneManager* mSceneManager;

};

#endif // GALAXYVIEW_H
