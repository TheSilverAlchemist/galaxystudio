#ifndef GALAXYGENERATOR_H
#define GALAXYGENERATOR_H

class Spiral;
class Star;
class ProbabilityMap;

class GalaxyGenerator
{

public:
//    GalaxyGenerator( ProbabilityMap *pMap, double galacticRadius, double galacticThickness, double armAlpha, double haloAlpha, double haloRadius, double globCount, double globSizeMean, double globSizeVar );
    GalaxyGenerator( double galacticRadius, double galacticThickness, double armAlpha, double nTurns, double haloAlpha, double haloRadius, double globCount, double globSizeMean, double globSizeVar );
    GalaxyGenerator( std::string fileName );
    ~GalaxyGenerator();

    /*
     *  Method:  void saveToFile( std::string fileName );
     *  Purpose: Save to file.
     */
    void saveToFile( std::string fileName );


    /*
     *  Method:  void seed( long s );
     *  Purpose: Sets a random seed.
     */
    void seed( long s );

    /*
     *  Method:  Star CreateStar();
     *  Purpose: Creates a star using the probability map.
     */
    Star CreateStar();

    double GetGalacticRadius() { return mGalacticRadius; }

protected:
//    ProbabilityMap *mProbabilityMap;// probability map to be used

    double mGalacticRadius;         // radius of the galaxy in light years
    double mGalacticThickness;      // thickness of the galactic disc in light years
    double mArmAlpha;               // drop-off value of the arms.
    double mNumberOfTurns;          // number of spiral turns

    double mHaloAlpha;              // drop-off value of the galactic halo. That is: haloProbability = haloRadius / ( haloAlpha * d^2 + haloRadius ) where d is the distance from the galactic centre.
    double mHaloRadius;             // radius of the halo in light years
    double mHaloMaxProb;            // the probability at the edge of the halo

    double mGlobCount;              // number of globular clusters. globular clusters can be found in both the halo and the disc. their sizes are normally distributed
    double mGlobSizeMean;           // mean size of globular clusters
    double mGlobSizeVar;            // variance of globular cluster size

    struct GlobularCluster {
        Ogre::Vector3 mCentre;
        double mSize;
    };

    std::vector<GlobularCluster> mClusterQuad[4]; // list of globular clusters in each quadrant

    /*
     *  Method:  void ComputeGlobularClusters();
     *  Purpose: Generates globular clusters.
     */
    void ComputeGlobularClusters();

    /*
     *  Method:  double GetSpiralProbability( Ogre::Vector3 pos );
     *  Purpose: Calculates the probability of a star being at pos using the spiral probability map.
     */
    double GetSpiralProbability( Ogre::Vector3 pos );

    /*
     *  Method:  double GetHaloProbability( Ogre::Vector3 pos );
     *  Purpose: Calculates the probability of a star being at pos using halo function.
     */
    double GetHaloProbability( Ogre::Vector3 pos );

    /*
     *  Method:  double GetGlobularProbability( Ogre::Vector3 pos );
     *  Purpose: Calculates the probability of a star being at pos using the globular cluster list.
     */
    double GetGlobularProbability( Ogre::Vector3 pos );

    /*
     *  Method:  double GetProbability( Ogre::Vector3 pos );
     *  Purpose: Calculates the probability of a star being at pos, selecting the maximum of spiral, halo, and globular probability.
     */
    double GetProbability( Ogre::Vector3 pos );

};

#endif // GALAXYGENERATOR_H
