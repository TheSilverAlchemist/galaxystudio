#ifndef GALAXYCONTAINER_H
#define GALAXYCONTAINER_H

#include <QtSql/QtSql>

class Body;
class Star;
class Planet;
class GalaxyGenerator;

class GalaxyContainer
{
protected:
    GalaxyGenerator *mGalaxyGenerator;
    unsigned int mStarCount;
    QString mGalaxyDirectory;

    QSqlDatabase mStarDatabase;

    double mRadialGranularity;
    double mLongitudeGranularity;
    double mLatitudeGranularity;
    unsigned int mNumberOfSectors;

public:

    struct SectorData {
        int mId;
        std::string mDesignation;
        double mRadiusStart;
        double mRadiusEnd;
        double mThetaStart;
        double mThetaEnd;
        double mPhiStart;
        double mPhiEnd;
        int mStarCount;
    };

    GalaxyContainer();
    GalaxyContainer( QString fileName );
    GalaxyContainer( unsigned int starCount, double rGranularity, double lonGranularity, double latGranularity, GalaxyGenerator *galaxyGenerator, QString fileName );
    ~GalaxyContainer();

    void InitDatabase();
    void InitNewDatabase();
    void PrepareTransaction();
    void GenerateStar();
    void CommitTransaction();
    void CloseDatabase();

    bool GetStarIndex( int index, Star *s );
    unsigned int GetStarCount();
    void GetPlanetsInSystem( Body *parentBody, std::vector<Planet> *pPlanet );

    bool AddPlanet( Planet &p, Body *pParent );

    double GetRadialGranularity();
    double GetLatitudeGranularity();
    double GetLongitudeGranularity();

    double GetGalacticRadius();

    void RedefineSectors( double rGranularity, double lonGranularity, double latGranularity );

    QSqlDatabase *GetStarDatabase();

    void GetSector( int sectorId, SectorData *pSector );
    void GetSector( int r, int t, int p, SectorData *pSector );
    int GetSectorId( int r, int t, int p );



};

#endif // GALAXYCONTAINER_H
