#ifndef SECTORVIEWDIALOG_H
#define SECTORVIEWDIALOG_H

#include <QDialog>

namespace Ui {
class SectorViewDialog;
}

class Star;
class SectorView;

class SectorViewDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SectorViewDialog(SectorView *pView, QWidget *parent = 0);
    ~SectorViewDialog();

    void SetSelectedStarData( Star *pStar );

private slots:
    void on_viewSystemButton_pressed();

    void on_leaveSectorButton_pressed();

    void on_designationEdit_editingFinished();

    void on_moveModeButton_pressed();

private:
    Ui::SectorViewDialog *ui;
    Star *mSelectedStar;
    SectorView *mSectorView;
};

#endif // SECTORVIEWDIALOG_H
