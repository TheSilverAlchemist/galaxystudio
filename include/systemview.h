#ifndef SYSTEMVIEW_H
#define SYSTEMVIEW_H




#include <OgrePrerequisites.h>
#include <QPoint>
#include <QHash>
#include <QEvent>
#include <QTime>

#include "Globals.h"
#include "systemviewdialog.h"

class GalaxyGenerator;
class Star;
class Planet;

class QKeyEvent;
class QMouseEvent;
class QWheelEvent;

namespace QtOgre{
    class GalaxyViewController;
};








class SystemView
{
protected:
    enum TextureIndex {
        TI_Front = 0,
        TI_Back,
        TI_Left,
        TI_Right,
        TI_Up,
        TI_Down
    };

    Ogre::TexturePtr mGalaxyBackground[6];


    enum InterfaceAction {
        MA_None = 0,
        MA_Select,
        MA_Rotate,
        MA_Zoom
    };

    InterfaceAction mAction;


    QtOgre::GalaxyViewController *mViewController;

    int mLastFrameWheelPos;
    int mCurrentWheelPos;
    QTime* mTime;

    int mLastFrameTime;
    int mCurrentTime;

    bool mIsFirstFrame;

    double mSimulationTimeStep;

    QHash<int, KeyStates> mKeyStates;
    QPoint mLastFrameMousePos;
    QPoint mCurrentMousePos;
    Ogre::Camera* mCamera;
    Ogre::SceneNode *mCameraSceneNode;
    Ogre::SceneManager* mSceneManager;

    Ogre::SceneNode *mTargetSceneNode;

    Ogre::SceneNode *mStarSceneNode;
    Star *mSelectedStar;
    Ogre::Vector3 mLocation;

    int mStarId;

    // planets in the system
    std::vector<Planet> mPlanets;
    std::map<std::string,Ogre::SceneNode*> mPlanetSceneNodes;
    std::map<std::string,Ogre::SceneNode*> mPlanetLabelSceneNodes;

    SystemViewDialog *mSystemView;

public:
    SystemView( QtOgre::GalaxyViewController *pView, int starId );
    ~SystemView();

    void initialise(void);
    void resume(void);
    void update(void);
    void halt(void);
    void shutdown(void);

    void InitialisePlanet(Planet *p);
    void ProcessPlanet(Planet *p, double elapsedTime);

    void onKeyPress(QKeyEvent* event);
    void onKeyRelease(QKeyEvent* event);

    void onMouseMove(QMouseEvent* event);
    void onMousePress(QMouseEvent* event);
    void onMouseRelease(QMouseEvent* event);
    void onWheel(QWheelEvent* event);

    QtOgre::GalaxyViewController *GetViewController();

    void SetupGalaxyBox();

    void GenerateRandomSystem();

    void SetSimulationTimeStep(double d);

};

#endif // SYSTEMVIEW_H
