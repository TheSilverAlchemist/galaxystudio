#ifndef PROBABILITYMAP_H
#define PROBABILITYMAP_H

class Spiral;

#define PROBABILITY_MAP_WIDTH 500

class ProbabilityMap
{

protected:
    double mProbMap[PROBABILITY_MAP_WIDTH][PROBABILITY_MAP_WIDTH];
    Spiral *mSpiral;
    double mInitialSpiralEnergy;
    double mArmThickness;

    struct SpiralArm {
        Ogre::Vector2 mStartPoint;
        double mStartPhase;
        double mLength;
        double mEnergy;
        double mInitialEnergy;
    };

    std::vector<SpiralArm> mSpiralArms;
    double mMaximumRadius;
    unsigned int mNumberOfArms;
    double mRadiusCoefficient;

    unsigned int mFilterWindowSize;

    void CalculateSpiral();
    void CalculateMap();

public:
    ProbabilityMap( Spiral *pSpiral, unsigned int nArms, double spiralEnergy, unsigned int windowSize, double armSize );
    ProbabilityMap( std::ifstream &fin );
    ~ProbabilityMap();

    double GetArmThickness() { return mArmThickness; }
    double GetInitialEnergy() { return mInitialSpiralEnergy; }
    void saveToFile( std::ofstream &fout );

    double GetProbability( unsigned int i, unsigned int j );

};

#endif // PROBABILITYMAP_H
