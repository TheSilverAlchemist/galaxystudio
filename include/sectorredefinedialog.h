#ifndef SECTORREDEFINEDIALOG_H
#define SECTORREDEFINEDIALOG_H

#include <QDialog>

namespace Ui {
class SectorRedefineDialog;
}

namespace QtOgre {
class GalaxyViewController;
}

class GalaxyView;

class SectorRedefineDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SectorRedefineDialog(GalaxyView *pView, QWidget *parent = 0);
    ~SectorRedefineDialog();
    
private slots:
    void on_okCancel_accepted();

private:
    Ui::SectorRedefineDialog *ui;
    GalaxyView *mGalaxyView;
};

#endif // SECTORREDEFINEDIALOG_H
