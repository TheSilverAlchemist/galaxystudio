#ifndef GALAXYGENERATORCONFIGDIALOG_H
#define GALAXYGENERATORCONFIGDIALOG_H

#include <QDialog>
#include <Application.h>
#include <QGraphicsPixmapItem>

namespace Ui {
class GalaxyGeneratorConfigDialog;
}

namespace QtOgre {
class GalaxyViewController;
}

class GalaxyView;

class Spiral;
class ProbabilityMap;

class GalaxyGeneratorConfigDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit GalaxyGeneratorConfigDialog(GalaxyView *controller, QWidget *parent = 0);
    ~GalaxyGeneratorConfigDialog();


private slots:
    void on_okCancel_apply();
    void on_okCancel_accepted();
    void on_okCancel_rejected();

    void on_spiralType_currentIndexChanged(int index);

    void on_maxTurns_sliderMoved(int position);

private:

    void GeneratePreview();

    void UpdatePreview();

    GalaxyView *mGalaxyView;
    Ui::GalaxyGeneratorConfigDialog *ui;

};

#endif // GALAXYGENERATORCONFIGDIALOG_H
