#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdarg.h>
#include <Ogre.h>

class CException {

public:
    std::string mMessage;
    std::string mFilename;
    unsigned long mLineNumber;

    CException( const char *fileName, unsigned long lineNumber, const char *format, ... ) : mFilename(fileName), mLineNumber(lineNumber) {

        char s[300];
        va_list args;
        va_start( args, format );
        vsprintf( s, format, args );
        va_end(args);
        mMessage = std::string(s);

    }
    ~CException() { }

};

#define THROW_EXCEPTION( str, ... )     throw CException( __FILE__, __LINE__, str, ##__VA_ARGS__ )


enum KeyStates
{
    KS_RELEASED,
    KS_PRESSED
};


#define MAX(a,b) (a<b?b:a)
#define MIN(a,b) (a>b?b:a)

#define GRAVITATIONAL_CONSTANT 6.673e-11

#define SOLAR_MASS          1.9891e30       // kilograms
#define SOLAR_LUMINOSITY    3.839e26        // watts
#define SOLAR_RADIUS        6.955e8         // metres

#define LIGHTYEAR           9.4605284e15    // metres

#define ASTRONOMICAL_UNIT   149.60e9        // metres

#define LIGHTYEAR_TO_METRES(ly) ly*LIGHTYEAR
#define METRES_TO_LIGHTYEAR(m)  m / LIGHTYEAR

#define LIGHTYEAR_TO_AU(ly) LIGHTYEAR_TO_METRES(ly)/ASTRONOMICAL_UNIT
#define AU_TO_LIGHTYEAR(au) METRES_TO_LIGHTYEAR(au * ASTRONOMICAL_UNIT)

#define METRES_TO_AU(m) m / ASTRONOMICAL_UNIT
#define AU_TO_METRES(au) au * ASTRONOMICAL_UNIT

#define MAX_PLANET_MASS   0.00095426575   // in solar masses
#define MAX_PLANET_RADIUS 0.00010279224   // in solar radii

#define STAR_INDEX_ID               0
#define STAR_INDEX_NAME             1
#define STAR_INDEX_GRADIUS          2
#define STAR_INDEX_LONG             3
#define STAR_INDEX_LAT              4
#define STAR_INDEX_AXISX            5
#define STAR_INDEX_AXISY            6
#define STAR_INDEX_AXISZ            7
#define STAR_INDEX_SPECCLASS        8
#define STAR_INDEX_MASS             9
#define STAR_INDEX_RADIUS           10
#define STAR_INDEX_LUM              11
#define STAR_INDEX_TEMP             12
#define STAR_INDEX_SECTORNAME       13


#define SECTOR_INDEX_ID             0
#define SECTOR_INDEX_DESIGN         1
#define SECTOR_INDEX_RAD_START      2
#define SECTOR_INDEX_RAD_END        3
#define SECTOR_INDEX_LONG_START     4
#define SECTOR_INDEX_LONG_END       5
#define SECTOR_INDEX_LAT_START      6
#define SECTOR_INDEX_LAT_END        7
#define SECTOR_INDEX_STAR_COUNT     8



#define PLANET_INDEX_ID             0
#define PLANET_INDEX_NAME           1
#define PLANET_INDEX_TYPE           2
#define PLANET_INDEX_MASS           3
#define PLANET_INDEX_RADIUS         4
#define PLANET_INDEX_ROT_SPEED      5
#define PLANET_INDEX_AXISX          6
#define PLANET_INDEX_AXISY          7
#define PLANET_INDEX_AXISZ          8
#define PLANET_INDEX_SEMIAXIS       9
#define PLANET_INDEX_ECCENTRICITY   10
#define PLANET_INDEX_LONGASCNODE    11
#define PLANET_INDEX_INCLINATION    12
#define PLANET_INDEX_ARGPERIAPSIS   13
#define PLANET_INDEX_PARENTNAME     14


#define SIMULATION_TIME_STEP 1e7


#endif // GLOBALS_H
