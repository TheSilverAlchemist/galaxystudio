#ifndef GALAXYVIEWCONTROLLER_H_
#define GALAXYVIEWCONTROLLER_H_

#include "GameLogic.h"
#include "Log.h"

#include <OgrePrerequisites.h>

#include <QHash>
#include <QTime>

//#include "star.h"
//#include "galaxyview.h"
//#include "sectorview.h"

class GalaxyContainer;
class GalaxyGenerator;
class GalaxyView;
class SectorView;
class SystemView;

namespace QtOgre
{


    class GalaxyViewController : public GameLogic
    {
    public:
        GalaxyViewController(void);

        void initialise(void);
        void update(void);
        void shutdown(void);

        void onKeyPress(QKeyEvent* event);
        void onKeyRelease(QKeyEvent* event);

        void onMouseMove(QMouseEvent* event);
        void onMousePress(QMouseEvent* event);
        void onMouseRelease(QMouseEvent* event);
        void onWheel(QWheelEvent* event);

        void createNewGalaxy( unsigned int starCount, double rGranularity, double lonGranularity, double latGranularity, GalaxyGenerator *pGen, QString fileName );
        void doGalaxyCreate();
        void doGalaxyLoad();

        GalaxyContainer *GetGalaxyContainer();
        Application *GetApplication();

        void GoToSector( int sectorId );
        void GoToSystem( int starId );
        void ReturnToGalaxy();

    private:
        GalaxyContainer *mCurrentGalaxy;
        QtOgre::Log* mAppLog;
        GalaxyView *mGalaxyView;
        SectorView *mSectorView;
        SystemView *mSystemView;

        enum ToolState {
            TS_Galaxy,
            TS_Sector,
            TS_System,
            TS_Planet
        };

        ToolState mState;

    };
}

#endif /*DEMOGAMELOGIC_H_*/
