#ifndef STAR_H
#define STAR_H




class Body {

public:
    Body();
    Body( std::string name, Ogre::Vector3 axis, double mass, double radius );
    virtual ~Body();

    std::string GetName();
    Ogre::Vector3 GetAxis();
    double GetMass();
    double GetRadius();

    void SetName( std::string strName );

protected:
    std::string mName;
    Ogre::Vector3 mAxis;
    double mMass;
    double mRadius;

};


class Star : public Body {

public:
    Star();
    Star( Ogre::Vector3 pos, double prob );
    Star( std::string name, Ogre::Vector3 axis, double mass, double radius, double luminosity, double temperature, Ogre::Vector3 location, const char *spectralClass );

    ~Star();

    double GetLuminosity();
    double GetTemperature();
    Ogre::Vector3 GetLocation();
    char *GetSpectralClass();

protected:
    double mLuminosity;
    double mTemperature;
    Ogre::Vector3 mLocation;
    char mSpectralClass[7];
    double mApparentMagnitude;

    static const double mStarClassDistribution[8];
    static const double mStarRadiusDistribution[8];
    static const double mStarMassDistribution[8];
    static const char mStarClasses[7];
    static const char *mStarSubclasses[];

};


class Planet;

class Orbit {

public:
    friend class Planet;
    Orbit();
    Orbit( double semimajorAxis, double e, double longAscNode, double i, double argPeriapsis, double bodyMass, Ogre::Vector3 centralBodyAxis );
    ~Orbit();

    Ogre::Vector3 CalculatePosition( double elapsedTime );

    void CalculateOrbitPoints( unsigned int nPoints, std::vector<Ogre::Vector3> *pPoints );

    // orbital parameters
    double GetSemimajorAxis();
    double GetEccentricity();
    double GetLongitudeOfAscendingNode();
    double GetInclination();
    double GetArgumentOfPeriapsis();

    // the ascending node
    Ogre::Vector3 GetAscendingNode();

    // orbital coordinate system
    Ogre::Matrix3 GetCoordinateSystem();

    // Orbital period (in seconds)
    double GetOrbitalPeriod();

    // Orbital veloctiy (in rad/sec)
    double GetOrbitalVelocity();

    // Solstice times
    double GetFirstSolsticeTime();
    double GetSecondSolsticeTime();

    // Equinox times
    double GetFirstEquinoxTime();
    double GetSecondEquinoxTime();

    // periapsis and apoapsis
    double GetPeriapsis();
    double GetApoapsis();

protected:

    double mCurrentTime;

    // orbital parameters
    double mSemimajorAxis;
    double mEccentricity;
    double mLongitudeOfAscendingNode;
    double mInclination;
    double mArgumentOfPeriapsis;

    // the ascending node
    Ogre::Vector3 mAscendingNode;

    // orbital coordinate system
    Ogre::Matrix3 mCoordinateSystem;    // 1st collumn is X, 2nd is Y, 3rd is Z

    // Orbital period (in seconds)
    double mOrbitalPeriod;

    // Orbital veloctiy (in rad/sec)
    double mOrbitalVelocity;

    // Solstice times
    double mSolsticeTimes[2];

    // Equinox times
    double mEquinoxTimes[2];

    // periapsis and apoapsis
    double mPeriapsis;
    double mApoapsis;

};



class Planet : public Body {

public:

    enum PlanetType {
        PT_Terrestrial = 0,
        PT_Dwarf,
        PT_Jovian
    };

    Planet();
    Planet( std::string strName, Ogre::Vector3 axis, double mass, double radius, PlanetType type, double rotationSpeed, Orbit o, std::string strParentName );
    ~Planet();

    PlanetType GetType();
    double GetRotationSpeed();
    Orbit *GetOrbit();
    std::string& GetParentName();

protected:
    PlanetType mType;
    double mRotationSpeed;
    Orbit mOrbit;
    std::string mParentName;

};



#endif // STAR_H
