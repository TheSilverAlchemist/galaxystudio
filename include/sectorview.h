#ifndef SECTORVIEW_H
#define SECTORVIEW_H


#include <OgrePrerequisites.h>
#include <QPoint>
#include <QHash>
#include <QEvent>
#include <QTime>

#include "sectorviewdialog.h"

#include "Globals.h"

class GalaxyGenerator;
class Star;

class QKeyEvent;
class QMouseEvent;
class QWheelEvent;

namespace QtOgre{
    class GalaxyViewController;
};




class SectorStar : public Star {

public:
    SectorStar( std::string name, Ogre::Vector3 location, Ogre::Vector3 axis, const char *spectralClass, double mass, double radius, double luminosity, double temperature, Ogre::Vector3 sectorOrigin, Ogre::SceneManager *pManager );
    ~SectorStar();

    Ogre::Entity *GetEntity();
    Ogre::SceneNode *GetSceneNode();

protected:
    Ogre::Entity *mEntity;
    Ogre::SceneNode *mSceneNode;

};



class SectorView
{

public:

    enum MovementMode {
        MM_ArcBall = 0,
        MM_StarshipMode
    };


    SectorView(int, QtOgre::GalaxyViewController *);

    void initialise(void);
    void resume(void);
    void update(void);
    void halt(void);
    void shutdown(void);

    void onKeyPress(QKeyEvent* event);
    void onKeyRelease(QKeyEvent* event);

    void onMouseMove(QMouseEvent* event);
    void onMousePress(QMouseEvent* event);
    void onMouseRelease(QMouseEvent* event);
    void onWheel(QWheelEvent* event);

    QtOgre::GalaxyViewController *GetViewController();

    void SetMovementMode( MovementMode );
    MovementMode GetMovementMode();

    std::string GetSectorDesignation();

    void SetupGalaxyBox();

protected:

    enum TextureIndex {
        TI_Front = 0,
        TI_Back,
        TI_Left,
        TI_Right,
        TI_Up,
        TI_Down
    };

    Ogre::TexturePtr mGalaxyBackground[6];

    MovementMode mMovementMode;

    enum InterfaceAction {
        MA_None = 0,
        MA_Select,
        MA_Rotate,
        MA_Zoom
    };

    InterfaceAction mAction;

    int mSectorId;
    QtOgre::GalaxyViewController *mViewController;

    std::vector<SectorStar> mSectorStars;
    std::map<Ogre::Entity*,unsigned int> mStarEntityHash;
    unsigned int mSelectedStarIndex;
    Ogre::SceneNode *mSelectionMarker;

    Ogre::RaySceneQuery *mRayQuery;

    int mLastFrameWheelPos;
    int mCurrentWheelPos;
    QTime* mTime;

    int mLastFrameTime;
    int mCurrentTime;

    bool mIsFirstFrame;

    Ogre::Vector3 mTargetPosition;
    Ogre::Vector3 mSectorOrigin;

    QHash<int, KeyStates> mKeyStates;
    QPoint mLastFrameMousePos;
    QPoint mCurrentMousePos;
    Ogre::Camera* mCamera;
    Ogre::SceneNode *mCameraSceneNode;
    Ogre::SceneManager* mSceneManager;

    SectorViewDialog *mViewDialog;

    Ogre::Vector3 mCameraDirection;
    Ogre::Real mCameraSpeed;
    Ogre::Real mCameraAcceleration;

};
#endif // SECTORVIEW_H
