#ifndef MAINCONTROLDIALOG_H
#define MAINCONTROLDIALOG_H

#include <QDialog>
#include <QtSql>
#include "galaxycontainer.h"
#include "galaxyviewcontroller.h"

namespace Ui {
class mainControlDialog;
}

class MainControlDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit MainControlDialog(GalaxyView *pView, QWidget *parent = 0);
    ~MainControlDialog();

    void setRadiusReading(double r);
    void setLatitudeReading(double lat);
    void setLongitudeReading(double lng);
    void setSectorName(QString n);

private slots:
    void on_newGalaxy_clicked();

    void on_openGalaxy_clicked();

    void on_sectorTable_clicked(const QModelIndex &index);

    void on_redefineSectorButton_pressed();

    void on_enterSectorButton_pressed();

private:
    Ui::mainControlDialog *ui;
    GalaxyView *mGalaxyView;
    QSqlTableModel *mSectorDataModel;

};

#endif // MAINCONTROLDIALOG_H
