#include <string>
#include <cmath>
#include <cstdio>
#include "spiral.h"
#include "Globals.h"

Spiral::Spiral( double maxTurns, double maxRadius ) : mParameter( 1 ), mMaximumTurns( maxTurns ), mMaximumRadius( maxRadius ) {

    mParameter = 1;

}

Spiral::~Spiral() {  }

std::string Spiral::getName() { return std::string("None"); }

double Spiral::toRadius( double angle ) { return 1; }


double Spiral::toAngle( double radius ) { return 1; }

double Spiral::getSpiralDistance( int x, int y ) {

    double r = sqrt( x*x + y*y );
    double t = atan2( y, x );
    unsigned int N = (unsigned int)( ( toAngle( r ) - t ) / ( 2 * M_PI ) );
    double t1 = t + 2 * M_PI * N;
    double t2 = t + 2 * M_PI * ( N + 1 );
    double d1 = fabs( toRadius( t1 ) - r );
    double d2 = fabs( toRadius( t2 ) - r );
    return ( d1 < d2 ? d1 : d2 );

}


void Spiral::setMaximumTurns( double maxTurns ) {

    mMaximumTurns = maxTurns;
    mParameter = 1;
    mParameter = mMaximumRadius / toRadius( mMaximumTurns * 2 * M_PI );

}



double Spiral::getMaximumTurns() {
    return mMaximumTurns;
}


double Spiral::getMaximumRadius() {
    return mMaximumRadius;
}



ArchimedesSpiral::ArchimedesSpiral( double maxTurns, double maxRadius ) : Spiral( maxTurns, maxRadius ) {
    mParameter = maxRadius / toRadius( mMaximumTurns * 2 * M_PI );
}
ArchimedesSpiral::~ArchimedesSpiral() {  }


std::string ArchimedesSpiral::getName() { return std::string("archimedes"); }


double ArchimedesSpiral::toRadius( double angle ) {
    return mParameter * angle;
}


double ArchimedesSpiral::toAngle( double radius ) {
    return radius / mParameter;
}


FermatSpiral::FermatSpiral( double maxTurns, double maxRadius ) : Spiral( maxTurns, maxRadius ) {
    mParameter = maxRadius / toRadius( mMaximumTurns * 2 * M_PI );
}
FermatSpiral::~FermatSpiral() {  }

std::string FermatSpiral::getName() { return std::string("fermat"); }

double FermatSpiral::toRadius( double angle ) {
    return mParameter * angle / sqrt( fabs( angle ) );
}



double FermatSpiral::toAngle( double radius ) {
    return pow( radius / mParameter, 2 );
}



NoisySpiral::NoisySpiral( double maxTurns, double maxRadius ) : Spiral( maxTurns, maxRadius ) {
    mParameter = maxRadius / toRadius( mMaximumTurns * 2 * M_PI );
    GenerateRandomNoise();
}


NoisySpiral::~NoisySpiral() { }

std::string NoisySpiral::getName() { return std::string("noisy"); }

double NoisySpiral::toRadius( double angle ) {
    double a = 0;
    for ( a = fabs(angle); a > 2*M_PI; a-=2*M_PI );
    int i = ( NOISE_LENGTH - 1 ) * a / (2*M_PI);
    return mParameter * ( angle + gRandomNoise[i] );

}



double NoisySpiral::toAngle( double radius ) {
    int i = ( NOISE_LENGTH - 1 ) * radius / (mParameter*2*M_PI);
    return radius / mParameter - gRandomNoise[i];
}



HyperbolicSpiral::HyperbolicSpiral( double maxTurns, double maxRadius ) : Spiral( maxTurns, maxRadius ) {
    mParameter = maxRadius / toRadius( mMaximumTurns * 2 * M_PI );
}


HyperbolicSpiral::~HyperbolicSpiral() { }


std::string HyperbolicSpiral::getName() { return std::string("hyperbolicsine"); }


double HyperbolicSpiral::toRadius( double angle ) {
    return mParameter * sinh( angle );
}



double HyperbolicSpiral::toAngle( double radius ) {
    return asinh( mParameter / radius );
}





SpiralFactory::SpiralFactory() { }
SpiralFactory::~SpiralFactory() { }

Spiral *SpiralFactory::CreateSpiralFunction( std::string spiralType, double maxTurns, double maxRadius ) {

    if ( spiralType == "archimedes" )
        return new ArchimedesSpiral( maxTurns, maxRadius );
    else if ( spiralType == "fermat" )
        return new FermatSpiral( maxTurns, maxRadius );
    else if ( spiralType == "noisy" )
        return new NoisySpiral( maxTurns, maxRadius );
    else if ( spiralType == "hyperbolicsine" )
        return new HyperbolicSpiral( maxTurns, maxRadius );

    return NULL;

}


