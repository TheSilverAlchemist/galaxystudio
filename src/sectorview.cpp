#include "OgreWidget.h"
#include <Application.h>

#include <OgreEntity.h>
#include <OgreRenderWindow.h>
#include <OgreResourceGroupManager.h>
#include <OgreRoot.h>
#include <OgreBillboard.h>
#include <OgreBillboardSet.h>
#include <OgreManualObject.h>
#include "MovableText.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QSettings>
#include <QMenuBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>

#include <cfloat>

#include "star.h"
#include "galaxycontainer.h"
#include "sectorview.h"
#include "galaxyviewcontroller.h"
#include "sectorviewdialog.h"



SectorStar::SectorStar( std::string name,
                        Ogre::Vector3 location,
                        Ogre::Vector3 axis,
                        const char *spectralClass,
                        double mass,
                        double radius,
                        double luminosity,
                        double temperature,
                        Ogre::Vector3 sectorOrigin,
                        Ogre::SceneManager *pManager ) : Star(name,axis,mass,radius,luminosity,temperature,location,spectralClass) {


    Ogre::Vector3 vStarPos = (Ogre::Vector3( cos(location.z)*cos(location.y), sin(location.z), cos(location.z)*sin(location.y) ) * location.x - sectorOrigin);

    vStarPos.x = -vStarPos.x;
    vStarPos.z = -vStarPos.z;

    mSceneNode = pManager->getRootSceneNode()->createChildSceneNode( name + std::string("SceneNode"), (vStarPos) );
    mEntity = pManager->createEntity( name, Ogre::SceneManager::PT_SPHERE );
    char str[10];
    sprintf(str,"class%c",mSpectralClass[0]);

    Ogre::MaterialPtr mPtr = Ogre::MaterialManager::getSingleton().getByName(str);
    mEntity->setMaterial( mPtr );

    mSceneNode->attachObject( mEntity );
    double r = METRES_TO_LIGHTYEAR(radius*SOLAR_RADIUS)*2000000;
    mSceneNode->setScale( r, r, r );

}




SectorStar::~SectorStar() { }



Ogre::Entity *SectorStar::GetEntity() {

    return mEntity;

}



Ogre::SceneNode *SectorStar::GetSceneNode() {

    return mSceneNode;

}




SectorView::SectorView(int sectorId, QtOgre::GalaxyViewController *vc) : mSectorId(sectorId), mViewController(vc)
{
}



void SectorView::initialise(void)
{

    // Create the generic scene manager
    mSceneManager = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC, "SectorSceneManager");

    //Set up scene
    mCamera = mSceneManager->createCamera("camera");

    mCameraSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode( "cameraSceneNode", Ogre::Vector3(500,0,0) );
    mCameraSceneNode->setDirection( Ogre::Vector3(-1,0,0), Ogre::Node::TS_WORLD );
    mCameraSceneNode->attachObject( mCamera );

    mCamera->setNearClipDistance(5);
    mCamera->setFarClipDistance(1000000);
    // Create one viewport, entire window
    Ogre::Viewport* vp = mViewController->GetApplication()->ogreRenderWindow()->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    mSceneManager->setAmbientLight( Ogre::ColourValue::White );

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio( Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));

    mSceneManager->setAmbientLight( Ogre::ColourValue( 1, 1, 1 ) );

    mTime = new QTime;
    mTime->start();

    mIsFirstFrame = true;

    // set up the sector map
    GalaxyContainer::SectorData sect;
    mViewController->GetGalaxyContainer()->GetSector( mSectorId, &sect );

    QSqlQuery q;
    char str[200];
    sprintf( str, "SELECT * FROM stars WHERE sectorName='%s'", sect.mDesignation.c_str() );
    q.exec(str);

    double cosPhi2 = cos((sect.mPhiEnd+sect.mPhiStart)/2);
    double sinPhi2 = sin((sect.mPhiEnd+sect.mPhiStart)/2);
    double cosTheta2 = cos((sect.mThetaEnd+sect.mThetaStart)/2);
    double sinTheta2 = sin((sect.mThetaEnd+sect.mThetaStart)/2);
    mTargetPosition = mSectorOrigin = Ogre::Vector3(cosPhi2*cosTheta2, sinPhi2, cosPhi2*sinTheta2) * (sect.mRadiusEnd+sect.mRadiusStart)/2;

    while( q.next() ) {

        mSectorStars.push_back( SectorStar( q.value(1).toString().toStdString(),
                                Ogre::Vector3( q.value(2).toDouble(), q.value(3).toDouble(), q.value(4).toDouble() ),
                                Ogre::Vector3( q.value(5).toDouble(), q.value(6).toDouble(), q.value(7).toDouble() ),
                                q.value(8).toString().toStdString().c_str(),
                                q.value(9).toDouble(),
                                q.value(10).toDouble(),
                                q.value(11).toDouble(),
                                q.value(12).toDouble(),
                                mSectorOrigin,
                                mSceneManager ) );

        mStarEntityHash[mSectorStars.back().GetEntity()] = mSectorStars.size()-1;

    };

    mSelectedStarIndex = UINT_MAX;

    mSelectionMarker = mSceneManager->createSceneNode("selectionMarkerSceneNode");
    Ogre::ManualObject *pSquare = mSceneManager->createManualObject("selectorMarker");
    mSelectionMarker->attachObject(pSquare);
    pSquare->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP);
    pSquare->position( -1, -1, 0 );
    pSquare->position(  1, -1, 0 );
    pSquare->position(  1,  1, 0 );
    pSquare->position( -1,  1, 0 );
    pSquare->index(0);
    pSquare->index(1);
    pSquare->index(2);
    pSquare->index(3);
    pSquare->index(0);
    pSquare->end();

    mViewDialog = new SectorViewDialog(this);
    mViewDialog->show();

    mCameraSpeed = 0;
    mCameraDirection = mCamera->getDirection();
    mCameraAcceleration = 200;
    mMovementMode = MM_ArcBall;

    SetupGalaxyBox();

}





void SectorView::resume() {

    if( mViewController->GetApplication()->ogreRenderWindow()->getNumViewports() != 0)
        mViewController->GetApplication()->ogreRenderWindow()->removeAllViewports();

    Ogre::Viewport* vp = mViewController->GetApplication()->ogreRenderWindow()->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

}




void SectorView::update(void)
{
    mLastFrameTime = mCurrentTime;
    mCurrentTime = mTime->elapsed();

    float timeElapsedInSeconds = (mCurrentTime - mLastFrameTime) / 1000.0f;

    if(!mIsFirstFrame)
    {

        if ( mMovementMode == MM_ArcBall ) {

            Ogre::Vector3 pos = mCameraSceneNode->getPosition();
            QPoint mouseDelta = mCurrentMousePos - mLastFrameMousePos;
            if ( mAction == MA_Rotate ) {
                if ( mouseDelta.x() != 0 || mouseDelta.y() != 0 ) {
                    Ogre::Vector3 vec = -pos.normalisedCopy();
                    Ogre::Quaternion rot1 = Ogre::Quaternion( Ogre::Degree(-mouseDelta.x() * 0.13), Ogre::Vector3::UNIT_Y ) * Ogre::Quaternion( Ogre::Degree(-mouseDelta.y() * 0.13), vec.crossProduct(Ogre::Vector3::UNIT_Y) );
                    pos = rot1 * pos;
                }
            } else if ( mAction == MA_Zoom ) {

                Ogre::Real dist = pos.length();
                if ( dist >= 1 ) {
                    dist += mouseDelta.y();
                    if ( dist < 1 )
                        dist = 1;
                    pos = pos.normalisedCopy() * dist;
                }

            }

            mCameraSceneNode->setPosition( pos );
            mCameraSceneNode->setDirection( -pos, Ogre::Node::TS_WORLD );

            mSelectionMarker->lookAt( pos, Ogre::Node::TS_PARENT );

        } else if ( mMovementMode == MM_StarshipMode ) {

            if ( mKeyStates[Qt::Key_Q] ) {
                mCameraSpeed -= mCameraAcceleration * timeElapsedInSeconds;
            } else if ( mKeyStates[Qt::Key_E] ) {
                mCameraSpeed += mCameraAcceleration * timeElapsedInSeconds;
            }
            if ( mCameraSpeed < 0 )
                mCameraSpeed = 0;

            Ogre::Real h, v;
            h = v = 0;
            Ogre::Matrix3 matLocalAxes = mCameraSceneNode->getLocalAxes();

            if ( mKeyStates[Qt::Key_A] ) {
                h = -1;
            } else if ( mKeyStates[Qt::Key_D] ) {
                h = 1;
            }

            if ( mKeyStates[Qt::Key_S] ) {
                v = 1;
            } else if ( mKeyStates[Qt::Key_W] ) {
                v = -1;
            }

            Ogre::Vector3 manipVec = h * matLocalAxes.GetColumn(0) - v * matLocalAxes.GetColumn(1);

            mCameraDirection += manipVec * timeElapsedInSeconds;
            mCameraDirection.normalise();

            mCameraSceneNode->translate( mCameraDirection * mCameraSpeed * timeElapsedInSeconds );
            mCameraSceneNode->setDirection( mCameraDirection, Ogre::Node::TS_WORLD );

            mSelectionMarker->lookAt( mCameraSceneNode->getPosition(), Ogre::Node::TS_PARENT );

        }

    }

    mLastFrameMousePos = mCurrentMousePos;
    mLastFrameWheelPos = mCurrentWheelPos;

    mIsFirstFrame = false;

    mRayQuery = mSceneManager->createRayQuery( Ogre::Ray() );
    mRayQuery->setSortByDistance(true,1);

}




void SectorView::halt(void) {

    if( mViewController->GetApplication()->ogreRenderWindow()->getNumViewports() != 0)
        mViewController->GetApplication()->ogreRenderWindow()->removeAllViewports();

    mViewDialog->hide();

}






void SectorView::shutdown(void)
{
    Ogre::MaterialManager::getSingleton().remove("galaxyBoxMaterial");
    Ogre::TextureManager::getSingleton().remove("galaxyLeft");
    Ogre::TextureManager::getSingleton().remove("galaxyFront");
    Ogre::TextureManager::getSingleton().remove("galaxyRight");
    Ogre::TextureManager::getSingleton().remove("galaxyBack");
    Ogre::TextureManager::getSingleton().remove("galaxyUp");
    Ogre::TextureManager::getSingleton().remove("galaxyDown");

    mSceneManager->clearScene();
    Ogre::Root::getSingleton().destroySceneManager(mSceneManager);
    delete mViewDialog;
}

void SectorView::onKeyPress(QKeyEvent* event)
{
    mKeyStates[event->key()] = KS_PRESSED;
}

void SectorView::onKeyRelease(QKeyEvent* event)
{
    mKeyStates[event->key()] = KS_RELEASED;
}

void SectorView::onMousePress(QMouseEvent* event)
{

    mCurrentMousePos = event->pos();
    mLastFrameMousePos = mCurrentMousePos;

    if ( mMovementMode == MM_ArcBall ) {

        if ( event->buttons() & Qt::LeftButton && event->buttons() & Qt::RightButton ) {
            mAction = MA_Zoom;
        } else if ( event->buttons() & Qt::RightButton ) {
            mAction = MA_Rotate;
        } else {
            mAction = MA_None;
        }

    }

    if ( event->buttons() & Qt::LeftButton && !( event->buttons() & Qt::RightButton ) ) {
        Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(mCurrentMousePos.x()/float(mCamera->getViewport()->getActualWidth()), mCurrentMousePos.y()/float(mCamera->getViewport()->getActualHeight()));
        mRayQuery->setRay(mouseRay);
        Ogre::RaySceneQueryResult &result = mRayQuery->execute();

        if ( mSelectionMarker->getParentSceneNode() ) {
            mSelectionMarker->getParentSceneNode()->removeChild(mSelectionMarker);
        }

        if ( result.begin() != result.end() ) {
            mSelectedStarIndex = mStarEntityHash[ reinterpret_cast<Ogre::Entity*>(result.begin()->movable) ];
            mSectorStars[mSelectedStarIndex].GetSceneNode()->addChild(mSelectionMarker);
            mSelectionMarker->setScale( Ogre::Vector3(1,1,1) * mSectorStars[mSelectedStarIndex].GetRadius() * 50 );
            mViewDialog->SetSelectedStarData( &mSectorStars[mSelectedStarIndex] );
        } else {
            mSelectedStarIndex = UINT_MAX;
        }
    }

}

void SectorView::onMouseRelease(QMouseEvent* event)
{
}

void SectorView::onMouseMove(QMouseEvent* event)
{
    mCurrentMousePos = event->pos();
}

void SectorView::onWheel(QWheelEvent* event)
{
    mCurrentWheelPos += event->delta();
}




QtOgre::GalaxyViewController *SectorView::GetViewController() {

    return mViewController;

}





void SectorView::SetMovementMode( SectorView::MovementMode mode ) {
    mMovementMode = mode;
}




SectorView::MovementMode SectorView::GetMovementMode() {
    return mMovementMode;
}



std::string SectorView::GetSectorDesignation() {
    GalaxyContainer::SectorData sect;
    mViewController->GetGalaxyContainer()->GetSector( mSectorId, &sect );
    return sect.mDesignation;
}



void SectorView::SetupGalaxyBox() {

    Ogre::SceneManager *pManager = Ogre::Root::getSingleton().createSceneManager( Ogre::ST_GENERIC, "boxMgr" );
    Ogre::Camera *cam = pManager->createCamera( "galaxyCamera" );
    pManager->setAmbientLight( Ogre::ColourValue(0,0,0,1) );
    cam->setPosition( mSectorOrigin/500 );
    cam->setNearClipDistance(5);
    cam->setFarClipDistance(1000000);
    cam->setAspectRatio(1);
    cam->setFOVy(Ogre::Radian(Ogre::Math::PI/2));

    GalaxyContainer::SectorData sect;
    mViewController->GetGalaxyContainer()->GetSector( mSectorId, &sect );

    QSqlQuery q;
    char str[300];

    sprintf( str, "SELECT gRadius,theta,phi,specClass,radius FROM stars WHERE sectorName!='%s'", sect.mDesignation.c_str() );
    q.exec(str);

    Ogre::BillboardSet *bbSet = pManager->createBillboardSet( "galaxy", mViewController->GetGalaxyContainer()->GetStarCount() );

    Ogre::ColourValue c; double x, y, z; char cl;
    while ( q.next() ) {

        cl = q.value(3).toString().toStdString()[0];
        if ( cl == 'O' )
            c = Ogre::ColourValue(    0,    0,    1, 1 );
        else if ( cl == 'B' )
            c = Ogre::ColourValue( 0.33, 0.33,    1, 1 );
        else if ( cl == 'A' )
            c = Ogre::ColourValue(  0.7,  0.7,    1, 1 );
        else if ( cl == 'F' )
            c = Ogre::ColourValue(    1,    1,    1, 1 );
        else if ( cl == 'G' )
            c = Ogre::ColourValue(    1,    1, 0.75, 1 );
        else if ( cl == 'K' )
            c = Ogre::ColourValue(    1,    1,    0, 1 );
        else if ( cl == 'M' )
            c = Ogre::ColourValue(    1,    0,    0, 1 );

        x = q.value(0).toDouble() * cos( q.value(2).toDouble() ) * cos( q.value(1).toDouble() );
        y = q.value(0).toDouble() * sin( q.value(2).toDouble() );
        z = q.value(0).toDouble() * cos( q.value(2).toDouble() ) * sin( q.value(1).toDouble() );
        bbSet->createBillboard( x/500, y/500, z/500, c )->setDimensions(q.value(4).toDouble()/8,q.value(4).toDouble()/8);
    }
    bbSet->setMaterialName("Flare");
    pManager->getRootSceneNode()->createChildSceneNode()->attachObject( bbSet );

    Ogre::Vector3 v1[6];
    v1[TI_Left]  =  Ogre::Vector3::UNIT_X;
    v1[TI_Front] =  Ogre::Vector3::UNIT_Z;
    v1[TI_Right] = -Ogre::Vector3::UNIT_X;
    v1[TI_Back]  = -Ogre::Vector3::UNIT_Z;
    v1[TI_Up]    =  Ogre::Vector3::UNIT_Y;
    v1[TI_Down]  = -Ogre::Vector3::UNIT_Y;

    std::string sName[6];

    sName[TI_Left]  = "Left";
    sName[TI_Front] = "Front";
    sName[TI_Right] = "Right";
    sName[TI_Back]  = "Back";
    sName[TI_Up]    = "Up";
    sName[TI_Down]  = "Down";

    Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create(
                std::string("galaxyBoxMaterial"),
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

    mat->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    Ogre::TextureUnitState *pState = mat->getTechnique(0)->getPass(0)->createTextureUnitState();

    std::string textureNames[6];

    for ( int vIndex = 0; vIndex < 6; vIndex++ ) {

        textureNames[vIndex] = std::string("galaxy")+sName[vIndex];

        cam->setDirection( v1[vIndex] );

        mGalaxyBackground[vIndex] = Ogre::TextureManager::getSingleton().createManual(
                    textureNames[vIndex],
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    Ogre::TEX_TYPE_2D,
                    2048, 2048, 0, Ogre::PF_X8R8G8B8, Ogre::TU_RENDERTARGET );

        Ogre::RenderTexture *rt = mGalaxyBackground[vIndex]->getBuffer()->getRenderTarget();
        rt->addViewport( cam );
        rt->getViewport(0)->setClearEveryFrame(true);
        rt->getViewport(0)->setBackgroundColour( Ogre::ColourValue::Black );
        rt->update();
        rt->removeAllViewports();

    }

    pState->setCubicTextureName( textureNames );

    Ogre::Root::getSingleton().destroySceneManager( pManager );

    mSceneManager->setSkyBox(true, "galaxyBoxMaterial");

}




