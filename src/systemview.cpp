
#include "OgreWidget.h"
#include <Application.h>

#include <OgreEntity.h>
#include <OgreRenderWindow.h>
#include <OgreResourceGroupManager.h>
#include <OgreRoot.h>
#include <OgreBillboard.h>
#include <OgreBillboardSet.h>
#include <OgreManualObject.h>
#include "MovableText.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QSettings>
#include <QMenuBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>
#include <QInputDialog>

#include <cfloat>

#include "star.h"
#include "galaxyviewcontroller.h"
#include "galaxycontainer.h"

#include "systemview.h"




SystemView::SystemView( QtOgre::GalaxyViewController *pView, int starId ) : mViewController(pView), mStarId(starId) {

}



SystemView::~SystemView() {

}




void SystemView::initialise(void) {

    // Create the generic scene manager
    mSceneManager = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC, "SystemSceneManager");

    //Set up scene
    mCamera = mSceneManager->createCamera("camera");

//    mCameraSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode( "cameraSceneNode", Ogre::Vector3(500,0,0) );
//    mCameraSceneNode->setDirection( Ogre::Vector3(-1,0,0), Ogre::Node::TS_WORLD );
//    mCameraSceneNode->attachObject( mCamera );

    mCamera->setNearClipDistance(0.1);
    mCamera->setFarClipDistance(1000000);
    mCamera->lookAt(0,0,0);
    mCamera->setPosition(Ogre::Vector3(10,0,0));
    // Create one viewport, entire window
    Ogre::Viewport* vp = mViewController->GetApplication()->ogreRenderWindow()->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    mSceneManager->setAmbientLight( Ogre::ColourValue::White );

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio( Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));

    mSceneManager->setAmbientLight( Ogre::ColourValue( 1, 1, 1 ) );

    mTime = new QTime;
    mTime->start();

    mIsFirstFrame = true;

    mSelectedStar = new Star();
    mViewController->GetGalaxyContainer()->GetStarIndex( mStarId, mSelectedStar );

    SetupGalaxyBox();

    mStarSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode( std::string("starSceneNode") );
    Ogre::Entity *entity = mSceneManager->createEntity( "star", Ogre::SceneManager::PT_SPHERE );
    char str[10];
    sprintf(str,"class%c",mSelectedStar->GetSpectralClass()[0]);

    Ogre::MaterialPtr mPtr = Ogre::MaterialManager::getSingleton().getByName(str);
    entity->setMaterial( mPtr );

    mStarSceneNode->attachObject( entity );
    double r = METRES_TO_AU(mSelectedStar->GetRadius()*SOLAR_RADIUS);
    mStarSceneNode->setScale( r, r, r );

    Ogre::MovableText *pText = new Ogre::MovableText(mSelectedStar->GetName()+"Label", mSelectedStar->GetName() );
    mPlanetLabelSceneNodes[ mSelectedStar->GetName() ] = mSceneManager->getRootSceneNode()->createChildSceneNode( mSelectedStar->GetName()+"LabelSceneNode" );
    mPlanetLabelSceneNodes[ mSelectedStar->GetName() ]->attachObject( pText );

    mViewController->GetGalaxyContainer()->GetPlanetsInSystem( mSelectedStar, &mPlanets );

    if ( !mPlanets.empty() ) {
        for ( std::vector<Planet>::iterator planetIt = mPlanets.begin(); planetIt != mPlanets.end(); planetIt++ ) {
            InitialisePlanet( &(*planetIt) );
        }
    }

    mTargetSceneNode = mStarSceneNode;

    mSystemView = new SystemViewDialog( this );
    mSystemView->show();

    mSimulationTimeStep = 1;

}


void SystemView::resume(void) {

    if( mViewController->GetApplication()->ogreRenderWindow()->getNumViewports() != 0)
        mViewController->GetApplication()->ogreRenderWindow()->removeAllViewports();

    Ogre::Viewport* vp = mViewController->GetApplication()->ogreRenderWindow()->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));
    mSystemView->show();

}



void SystemView::update(void) {
    mLastFrameTime = mCurrentTime;
    mCurrentTime = mTime->elapsed();

    float timeElapsedInSeconds = (mCurrentTime - mLastFrameTime) / 1000.0f;

    if(!mIsFirstFrame)
    {

        double d = mCamera->getPosition().length() * 0.01;
        mPlanetLabelSceneNodes[mSelectedStar->GetName()]->setScale( d, d, d );
        Ogre::Vector3 dir = -mCamera->getPosition().crossProduct(Ogre::Vector3::UNIT_Y).normalisedCopy();
        mPlanetLabelSceneNodes[mSelectedStar->GetName()]->setPosition( mStarSceneNode->getPosition() + dir );

        for ( std::vector<Planet>::iterator planetIt = mPlanets.begin(); planetIt != mPlanets.end(); planetIt++ ) {
            ProcessPlanet( &(*planetIt), timeElapsedInSeconds );
        }

        Ogre::Vector3 pos = mCamera->getPosition() - mTargetSceneNode->getPosition();
        QPoint mouseDelta = mCurrentMousePos - mLastFrameMousePos;
        if ( mAction == MA_Rotate ) {
            if ( mouseDelta.x() != 0 || mouseDelta.y() != 0 ) {
                Ogre::Vector3 vec = -pos.normalisedCopy();
                Ogre::Quaternion rot1 = Ogre::Quaternion( Ogre::Degree(-mouseDelta.x() * 0.13), Ogre::Vector3::UNIT_Y ) * Ogre::Quaternion( Ogre::Degree(-mouseDelta.y() * 0.13), vec.crossProduct(Ogre::Vector3::UNIT_Y) );
                pos = rot1 * pos;
            }
        } else if ( mAction == MA_Zoom ) {

            Ogre::Real dist = pos.length();
            if ( dist >= 1 ) {
                dist += mouseDelta.y()/10.0;
                if ( dist < 1 )
                    dist = 1.5;
                pos = pos.normalisedCopy() * dist;
            }

        }

        mCamera->setPosition( pos + mTargetSceneNode->getPosition() );
        mCamera->lookAt( mTargetSceneNode->getPosition() );

    }

    mLastFrameMousePos = mCurrentMousePos;
    mLastFrameWheelPos = mCurrentWheelPos;

    mIsFirstFrame = false;

}



void SystemView::halt(void) {

    if( mViewController->GetApplication()->ogreRenderWindow()->getNumViewports() != 0)
        mViewController->GetApplication()->ogreRenderWindow()->removeAllViewports();
    mSystemView->hide();

}



void SystemView::shutdown(void) {

    Ogre::MaterialManager::getSingleton().remove("galaxyBoxMaterial");
    Ogre::TextureManager::getSingleton().remove("galaxyLeft");
    Ogre::TextureManager::getSingleton().remove("galaxyFront");
    Ogre::TextureManager::getSingleton().remove("galaxyRight");
    Ogre::TextureManager::getSingleton().remove("galaxyBack");
    Ogre::TextureManager::getSingleton().remove("galaxyUp");
    Ogre::TextureManager::getSingleton().remove("galaxyDown");

    mSceneManager->clearScene();
    Ogre::Root::getSingleton().destroySceneManager(mSceneManager);

    delete mSelectedStar;

}




void SystemView::InitialisePlanet(Planet *p) {

    mPlanetSceneNodes[ p->GetName() ] = mSceneManager->getRootSceneNode()->createChildSceneNode( p->GetName()+"SceneNode" );
    Ogre::Entity *entity = mSceneManager->createEntity( p->GetName()+"Entity", Ogre::SceneManager::PT_SPHERE );
    // TODO: ADD PLANET TEXTURE
    mPlanetSceneNodes[ p->GetName() ]->attachObject( entity );
//    double r = METRES_TO_AU(p->GetRadius()*SOLAR_RADIUS);
    double r = METRES_TO_AU(mSelectedStar->GetRadius()*SOLAR_RADIUS);
    mPlanetSceneNodes[ p->GetName() ]->setScale(r,r,r);

    Ogre::MovableText *pText = new Ogre::MovableText(p->GetName()+"Label", p->GetName() );
    mPlanetLabelSceneNodes[ p->GetName() ] = mSceneManager->getRootSceneNode()->createChildSceneNode( p->GetName()+"LabelSceneNode" );
    mPlanetLabelSceneNodes[ p->GetName() ]->attachObject( pText );

}



void SystemView::ProcessPlanet(Planet *p, double elapsedTime) {

    Ogre::Vector3 newPos = METRES_TO_AU(p->GetOrbit()->CalculatePosition( elapsedTime * mSimulationTimeStep * SIMULATION_TIME_STEP ));
    if ( p->GetParentName() != mSelectedStar->GetName() )
        newPos += mPlanetSceneNodes[ p->GetParentName() ]->getPosition();
    mPlanetSceneNodes[p->GetName()]->setPosition( newPos );
    Ogre::Vector3 pos = mPlanetSceneNodes[p->GetName()]->getPosition() - mCamera->getPosition();
    double d = pos.length()*0.01;
    mPlanetLabelSceneNodes[p->GetName()]->setScale( d, d, d );
    Ogre::Vector3 dir = -pos.crossProduct(Ogre::Vector3::UNIT_Y).normalisedCopy();
    mPlanetLabelSceneNodes[p->GetName()]->setPosition( newPos+dir );

}



void SystemView::onKeyPress(QKeyEvent* event)
{
    mKeyStates[event->key()] = KS_PRESSED;
}

void SystemView::onKeyRelease(QKeyEvent* event)
{
    mKeyStates[event->key()] = KS_RELEASED;
}

void SystemView::onMousePress(QMouseEvent* event)
{

    mCurrentMousePos = event->pos();
    mLastFrameMousePos = mCurrentMousePos;

    if ( event->buttons() & Qt::LeftButton && event->buttons() & Qt::RightButton ) {
        mAction = MA_Zoom;
    } else if ( event->buttons() & Qt::RightButton ) {
        mAction = MA_Rotate;
    } else if ( event->buttons() & Qt::LeftButton ) {
        mAction = MA_Select;
    } else {
        mAction = MA_None;
    }


}

void SystemView::onMouseRelease(QMouseEvent* event)
{
}

void SystemView::onMouseMove(QMouseEvent* event)
{
    mCurrentMousePos = event->pos();
}

void SystemView::onWheel(QWheelEvent* event)
{
    mCurrentWheelPos += event->delta();
}



QtOgre::GalaxyViewController *SystemView::GetViewController() {
    return mViewController;
}







void SystemView::SetupGalaxyBox() {

    double cosPhi2 = cos(mSelectedStar->GetLocation().z);
    double sinPhi2 = sin(mSelectedStar->GetLocation().z);
    double cosTheta2 = cos(mSelectedStar->GetLocation().y);
    double sinTheta2 = sin(mSelectedStar->GetLocation().y);
    mLocation = Ogre::Vector3(cosPhi2*cosTheta2, sinPhi2, cosPhi2*sinTheta2) * mSelectedStar->GetLocation().x;

    Ogre::SceneManager *pManager = Ogre::Root::getSingleton().createSceneManager( Ogre::ST_GENERIC, "boxMgr" );
    Ogre::Camera *cam = pManager->createCamera( "galaxyCamera" );
    pManager->setAmbientLight( Ogre::ColourValue(0,0,0,1) );
    cam->setPosition( mLocation/500 );
    cam->setNearClipDistance(5);
    cam->setFarClipDistance(1000000);
    cam->setAspectRatio(1);
    cam->setFOVy(Ogre::Radian(Ogre::Math::PI/2));

    Ogre::Vector3 coordY = mSelectedStar->GetLocation().normalisedCopy();
    Ogre::Vector3 coordX = coordY.crossProduct( Ogre::Vector3::UNIT_Y ).normalisedCopy();
    Ogre::Vector3 coordZ = coordX.crossProduct(coordY).normalisedCopy();

    QSqlQuery q;
    char str[300];

    sprintf( str, "SELECT gRadius,theta,phi,specClass,radius FROM stars WHERE name!='%s'", mSelectedStar->GetName().c_str() );
    q.exec(str);

    Ogre::BillboardSet *bbSet = pManager->createBillboardSet( "galaxy", mViewController->GetGalaxyContainer()->GetStarCount() );

    Ogre::ColourValue c; double x, y, z; char cl;
    while ( q.next() ) {

        cl = q.value(3).toString().toStdString()[0];
        if ( cl == 'O' )
            c = Ogre::ColourValue(    0,    0,    1, 1 );
        else if ( cl == 'B' )
            c = Ogre::ColourValue( 0.33, 0.33,    1, 1 );
        else if ( cl == 'A' )
            c = Ogre::ColourValue(  0.7,  0.7,    1, 1 );
        else if ( cl == 'F' )
            c = Ogre::ColourValue(    1,    1,    1, 1 );
        else if ( cl == 'G' )
            c = Ogre::ColourValue(    1,    1, 0.75, 1 );
        else if ( cl == 'K' )
            c = Ogre::ColourValue(    1,    1,    0, 1 );
        else if ( cl == 'M' )
            c = Ogre::ColourValue(    1,    0,    0, 1 );

        x = q.value(0).toDouble() * cos( q.value(2).toDouble() ) * cos( q.value(1).toDouble() );
        y = q.value(0).toDouble() * sin( q.value(2).toDouble() );
        z = q.value(0).toDouble() * cos( q.value(2).toDouble() ) * sin( q.value(1).toDouble() );
        bbSet->createBillboard( x/500, y/500, z/500, c )->setDimensions(q.value(4).toDouble()/8,q.value(4).toDouble()/8);
    }
    bbSet->setMaterialName("Flare");
    pManager->getRootSceneNode()->createChildSceneNode()->attachObject( bbSet );

    Ogre::Vector3 v1[6];
    v1[TI_Left]  =  coordX;
    v1[TI_Front] =  coordZ;
    v1[TI_Right] = -coordX;
    v1[TI_Back]  = -coordZ;
    v1[TI_Up]    =  coordY;
    v1[TI_Down]  = -coordY;

    std::string sName[6];

    sName[TI_Left]  = "Left";
    sName[TI_Front] = "Front";
    sName[TI_Right] = "Right";
    sName[TI_Back]  = "Back";
    sName[TI_Up]    = "Up";
    sName[TI_Down]  = "Down";

    Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create(
                std::string("galaxyBoxMaterial"),
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

    mat->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    Ogre::TextureUnitState *pState = mat->getTechnique(0)->getPass(0)->createTextureUnitState();

    std::string textureNames[6];

    for ( int vIndex = 0; vIndex < 6; vIndex++ ) {

        textureNames[vIndex] = std::string("galaxy")+sName[vIndex];

        cam->setDirection( v1[vIndex] );

        mGalaxyBackground[vIndex] = Ogre::TextureManager::getSingleton().createManual(
                    textureNames[vIndex],
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    Ogre::TEX_TYPE_2D,
                    2048, 2048, 0, Ogre::PF_X8R8G8B8, Ogre::TU_RENDERTARGET );

        Ogre::RenderTexture *rt = mGalaxyBackground[vIndex]->getBuffer()->getRenderTarget();
        rt->addViewport( cam );
        rt->getViewport(0)->setClearEveryFrame(true);
        rt->getViewport(0)->setBackgroundColour( Ogre::ColourValue::Black );
        rt->update();
        rt->removeAllViewports();

    }

    pState->setCubicTextureName( textureNames );

    Ogre::Root::getSingleton().destroySceneManager( pManager );

    mSceneManager->setSkyBox(true, "galaxyBoxMaterial");

}





void SystemView::GenerateRandomSystem() {

    bool ok;
    QString text = QInputDialog::getText(NULL, QString("Random planet"),
                                      QString("Enter planet name:"), QLineEdit::Normal,
                                      QDir::home().dirName(), &ok);
    if (!ok || text.isEmpty())
        return;

    std::string strName = text.toStdString();
    Ogre::Vector3 axis = Ogre::Vector3( Ogre::Math::RangeRandom(-1,1), Ogre::Math::RangeRandom(-1,1), Ogre::Math::RangeRandom(-1,1) ).normalisedCopy();
    double mass = Ogre::Math::RangeRandom(0,MAX_PLANET_MASS*SOLAR_MASS);
    double radius = Ogre::Math::RangeRandom(0,MAX_PLANET_RADIUS*SOLAR_RADIUS);
    double density = 3 * mass / ( 4 * M_PI * radius * radius * radius * 1e12 );
    Planet::PlanetType type;
    if ( density > 3 )
        type = Planet::PT_Terrestrial;
    else
        type = Planet::PT_Jovian;


    double rotationSpeed = 2*M_PI / Ogre::Math::RangeRandom(0,172800);
    double semimajorAxis = Ogre::Math::RangeRandom(0,50)*ASTRONOMICAL_UNIT;
    double e = Ogre::Math::RangeRandom(0,0.9);
    double longAscNode = Ogre::Math::RangeRandom(0,M_PI);
    double i = Ogre::Math::RangeRandom(0,M_PI/2);
    double argPeriapsis = Ogre::Math::RangeRandom(0,M_PI);
    double bodyMass = mass + mSelectedStar->GetMass();
    Ogre::Vector3 centralBodyAxis = mSelectedStar->GetAxis();

    Planet p = Planet(
                    strName,
                    axis,
                    mass,
                    radius,
                    type,
                    rotationSpeed,
                    Orbit(
                        semimajorAxis,
                        e,
                        longAscNode,
                        i,
                        argPeriapsis,
                        bodyMass,
                        centralBodyAxis
                        ),
                    mSelectedStar->GetName()
                );

    if ( !mViewController->GetGalaxyContainer()->AddPlanet( p, mSelectedStar ) ) {
        return;
    }

    mPlanets.push_back( p );

    InitialisePlanet( &p );

}






void SystemView::SetSimulationTimeStep(double d) {
    mSimulationTimeStep = d;
}



