
#include <Ogre.h>
#include <string>
#include <climits>
#include <cfloat>
#include <cmath>

#include <fstream>

#include "Globals.h"
#include "star.h"
#include "galaxygenerator.h"

GalaxyGenerator::GalaxyGenerator(
        double galacticRadius,
        double galacticThickness,
        double armAlpha,
        double nTurns,
        double haloAlpha,
        double haloRadius,
        double globCount,
        double globSizeMean,
        double globSizeVar )
    : mGalacticRadius( galacticRadius ),
      mGalacticThickness( galacticThickness ),
      mArmAlpha( armAlpha ),
      mNumberOfTurns( nTurns ),
      mHaloAlpha( haloAlpha ),
      mHaloRadius( haloRadius ),
      mGlobCount( globCount ),
      mGlobSizeMean( globSizeMean ),
      mGlobSizeVar( globSizeVar ) {

    ComputeGlobularClusters();

}


GalaxyGenerator::GalaxyGenerator( std::string fileName ) {

    std::ifstream fin;
    fin.open( fileName.c_str() );

    fin >> mGalacticRadius
        >> mGalacticThickness
        >> mArmAlpha
        >> mHaloAlpha
        >> mNumberOfTurns
        >> mHaloRadius
        >> mGlobCount
        >> mGlobSizeMean
        >> mGlobSizeVar;

    unsigned int glob;
    GlobularCluster g;
    for ( unsigned int i = 0; i < mGlobCount; i++ ) {

        fin >> g.mCentre.x >> g.mCentre.y >> g.mCentre.z >> g.mSize;
        glob = 0;
        if ( g.mCentre.x < 0 && g.mCentre.z > 0 )
            glob = 1;
        else if ( g.mCentre.x < 0 && g.mCentre.z < 0 )
            glob = 2;
        else if ( g.mCentre.x > 0 && g.mCentre.z < 0 )
            glob = 3;

        mClusterQuad[glob].push_back( g );

    }

//    mProbabilityMap = new ProbabilityMap( fin );
    fin.close();

}


GalaxyGenerator::~GalaxyGenerator() {

//    delete mProbabilityMap;

    mClusterQuad[0].clear();
    mClusterQuad[1].clear();
    mClusterQuad[2].clear();
    mClusterQuad[3].clear();

}


/*
 *  Method:  void saveToFile( std::string fileName );
 *  Purpose: Save to file.
 */
void GalaxyGenerator::saveToFile( std::string fileName ) {

    std::ofstream fout;
    fout.open( fileName.c_str() );

    fout << mGalacticRadius << " "
         << mGalacticThickness << " "
         << mArmAlpha << " "
         << mNumberOfTurns << " "
         << mHaloAlpha << " "
         << mHaloRadius << " "
         << mGlobCount << " "
         << mGlobSizeMean << " "
         << mGlobSizeVar << "\n";

    for ( int i = 0; i < 4; i++ )
        for ( std::vector<GlobularCluster>::iterator it = mClusterQuad[i].begin(); it != mClusterQuad[i].end(); it++ )
            fout << it->mCentre.x << " " << it->mCentre.y << " " << it->mCentre.z << " " << it->mSize << "\n";

//    mProbabilityMap->saveToFile( fout );
    fout.close();

}


/*
 *  Method:  void seed( long s );
 *  Purpose: Sets a random seed.
 */
void GalaxyGenerator::seed( long s ) {
    srand(s);
}



/*
 *  Method:  Star *CreateStar();
 *  Purpose: Creates a star using the probability map.
 */
Star GalaxyGenerator::CreateStar() {

    Ogre::Vector3 pos;
    double prob = 0;
//    double r = 0;
//    double t = 0;
//    double p = 0;

    do
    {
//        pos.x = sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX ) * mGalacticRadius / 2;
//        pos.y = sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX ) * mGalacticRadius / 4;
//        pos.z = sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX ) * mGalacticRadius / 2;

//        r = mGalacticRadius * Ogre::Math::RangeRandom(-1,1);
//        t = M_PI * Ogre::Math::RangeRandom(0,2);
//        p = M_PI * Ogre::Math::RangeRandom(-0.5,0.5);
//        if ( r < 0 ) {
//            t -= M_PI;
//            p = -p;
//            r = -r;
//        }
//        pos.x = r * cos(p) * cos(t);
//        pos.y = r * sin(p);
//        pos.z = r * cos(p) * sin(t);

        pos.x = mGalacticRadius * Ogre::Math::RangeRandom(-1.5,1.5);
        pos.z = mGalacticRadius * Ogre::Math::RangeRandom(-1.5,1.5);
        pos.y = mGalacticRadius * Ogre::Math::RangeRandom(-1.5,1.5);
        prob = GetProbability( pos );

    } while( rand()/(double)RAND_MAX > prob );

    return Star( pos, prob );

}



/*
 *  Method:  void ComputeGlobularClusters();
 *  Purpose: Generates globular clusters.
 */
void GalaxyGenerator::ComputeGlobularClusters() {

    Ogre::Vector3 pos;
    int g = 0;
    GlobularCluster cluster;
    for ( unsigned int i = 0; i < mGlobCount; i++ ) {

        pos.x = sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX );
        pos.y = sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX );
        pos.z = sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX );
        pos *= mGalacticRadius / 2;

        g = 0;
        if ( pos.x < 0 && pos.z > 0 )
            g = 1;
        else if ( pos.x < 0 && pos.z < 0 )
            g = 2;
        else if ( pos.x > 0 && pos.z < 0 )
            g = 3;

        cluster.mCentre = pos;
        cluster.mSize = mGlobSizeMean + mGlobSizeVar * sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX );
        cluster.mSize *= cluster.mSize;
        mClusterQuad[g].push_back( cluster );

    }

}

/*
 *  Method:  double GetSpiralProbability( Ogre::Vector3 pos );
 *  Purpose: Calculates the probability of a star being at pos using the spiral probability map.
 */
double GalaxyGenerator::GetSpiralProbability( Ogre::Vector3 pos ) {

//    double t = mGalacticThickness / 2;
//    unsigned int halfSize = PROBABILITY_MAP_WIDTH/2;
//    unsigned int i = (unsigned int)( ( pos.x / mGalacticRadius + 1 ) * halfSize );
//    unsigned int j = (unsigned int)( ( pos.z / mGalacticRadius + 1 ) * halfSize );

//    if ( i < PROBABILITY_MAP_WIDTH && j < PROBABILITY_MAP_WIDTH )
//        return mProbabilityMap->GetProbability(i,j) * t / ( mArmAlpha * pos.y * pos.y + t );

//    return 0;

    double d = pos.length();
    if ( d > mGalacticRadius )
        return 0;
    double t = mGalacticThickness;
    double n = mGalacticRadius / ( mNumberOfTurns * 2 * M_PI );
    double p = exp( -2*d*d/(mGalacticRadius*mGalacticRadius) );

    return p * fabs( 0.1+0.9*cos( 2*sqrt(pos.z*pos.z+pos.x*pos.x)/n + atan2(pos.z,pos.x) ) ) * t / ( mArmAlpha * pos.y * pos.y + t );

}

/*
 *  Method:  double GetHaloProbability( Ogre::Vector3 pos );
 *  Purpose: Calculates the probability of a star being at pos using halo function.
 */
double GalaxyGenerator::GetHaloProbability( Ogre::Vector3 pos ) {

    return exp(-mHaloAlpha*pos.squaredLength()*4/(mHaloRadius*mHaloRadius)) * 0.9;

}

/*
 *  Method:  double GetGlobularProbability( Ogre::Vector3 pos );
 *  Purpose: Calculates the probability of a star being at pos using the globular cluster list.
 */
double GalaxyGenerator::GetGlobularProbability( Ogre::Vector3 pos ) {

    int g = 0;
    if ( pos.x < 0 && pos.z > 0 )
        g = 1;
    else if ( pos.x < 0 && pos.z < 0 )
        g = 2;
    else if ( pos.x > 0 && pos.z < 0 )
        g = 3;

    double dMin = DBL_MAX, d, s;

    for ( std::vector<GlobularCluster>::iterator it = mClusterQuad[g].begin(); it != mClusterQuad[g].end(); it++ ) {

        d = pos.squaredDistance(it->mCentre);
        if ( d < dMin ) {
            dMin = d;
            s = it->mSize;
        }

    }

    return exp(-mHaloAlpha*dMin*2/s) * 0.9;

}

/*
 *  Method:  double GetProbability( Ogre::Vector3 pos );
 *  Purpose: Calculates the probability of a star being at pos, selecting the maximum of spiral, halo, and globular probability.
 */
double GalaxyGenerator::GetProbability( Ogre::Vector3 pos ) {

    std::vector<double> probs;
    probs.push_back( GetSpiralProbability( pos ) );
    probs.push_back( GetHaloProbability( pos ) );
    probs.push_back( GetGlobularProbability( pos ) );
    std::vector<double>::iterator it = std::max_element( probs.begin(), probs.end() );
    return *it;

}



