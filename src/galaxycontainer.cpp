#include <Ogre.h>

#include <QProgressDialog>
#include <QVBoxLayout>

#include <QtSql/QtSql>
#include <QDir>
#include <QMessageBox>

#include <climits>
#include <fstream>
#include <string>

#include "Globals.h"
#include "star.h"
#include "galaxygenerator.h"
#include "galaxycontainer.h"

GalaxyContainer::GalaxyContainer()
{
}


GalaxyContainer::GalaxyContainer( QString fileName ) : mGalaxyDirectory( fileName ) {

    InitDatabase();
    mGalaxyGenerator = new GalaxyGenerator( mGalaxyDirectory.toStdString() + std::string( "/generator.config" ) );

    SectorData s;
    GetSector(1,&s);
    mRadialGranularity = fabs(s.mRadiusEnd - s.mRadiusStart);
    mLongitudeGranularity = fabs(s.mThetaEnd - s.mThetaStart);
    mLatitudeGranularity = fabs(s.mPhiEnd - s.mPhiStart);

}



GalaxyContainer::GalaxyContainer( unsigned int starCount, double rGranularity, double lonGranularity, double latGranularity, GalaxyGenerator *galaxyGenerator, QString fileName ) :
    mStarCount(starCount),
    mGalaxyGenerator(galaxyGenerator),
    mGalaxyDirectory(fileName),
    mRadialGranularity(rGranularity),
    mLongitudeGranularity(lonGranularity),
    mLatitudeGranularity(latGranularity) {

    mNumberOfSectors = (2*M_PI*M_PI*mGalaxyGenerator->GetGalacticRadius()) / ( mRadialGranularity * mLongitudeGranularity * mLatitudeGranularity );

    InitNewDatabase();

    QVBoxLayout* layout = new QVBoxLayout;
    QWidget *win = new QWidget;
    QProgressDialog *progress = new QProgressDialog( "Generating galaxy...", "", 0, 100 );

    progress->setWindowModality(Qt::WindowModal);
    layout->addWidget(progress,Qt::AlignCenter);
    win->setLayout(layout);

    progress->setValue( 0 );
    win->show();

    PrepareTransaction();

    for ( unsigned int i = 0; i < mStarCount; i++ ) {

        GenerateStar();

        progress->setValue( 100 * (double)i / (double)mStarCount );
        win->show();

    }

    CommitTransaction();



    mGalaxyGenerator->saveToFile( mGalaxyDirectory.toStdString() + std::string( "/generator.config" ) );

    delete progress;
    delete layout;
    delete win;

}


GalaxyContainer::~GalaxyContainer() {

//    CloseDatabase();
    delete mGalaxyGenerator;
    mStarDatabase.close();

}



void GalaxyContainer::InitDatabase() {

    QDir dir(mGalaxyDirectory);
    if( !dir.exists() ) {
        dir.mkpath(".");
    }

    mStarDatabase = QSqlDatabase::addDatabase("QSQLITE");
    mStarDatabase.setDatabaseName( mGalaxyDirectory+QString("/starCatalogue") );
    if ( !mStarDatabase.open() )
        THROW_EXCEPTION( "Could not create star catalogue: %s. QSqlError: %s", mGalaxyDirectory.toStdString().c_str(), mStarDatabase.lastError().text().toStdString().c_str() );

}



void GalaxyContainer::InitNewDatabase() {

    InitDatabase();

    QSqlQuery q;

    // create the table for stars
    if ( !q.exec( QLatin1String( "CREATE TABLE stars(id integer primary key, name varchar, gRadius real, theta real, phi real, axisX real, axisY real, axisZ real, specClass varchar, mass real, radius real, luminosity real, temperature real, sectorName varchar )" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // create the table for sectors
    if ( !q.exec( QLatin1String( "CREATE TABLE sectors(id integer primary key, designation varchar, rStart real, rEnd real, thetaStart real, thetaEnd real, phiStart real, phiEnd real, starCount int)" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // create the table for planets
    if ( !q.exec( QLatin1String( "CREATE TABLE planets(id integer primary key, name varchar, type varchar, mass real, radius real, rotationSpeed real, axisX real, axisY real, axisZ real, semimajorAxis real, eccentricity real, longitudeAscendingNode real, inclination real, argumentPeriapsis real, parentName varchar)" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // create the table for body counts
    if ( !q.exec( QLatin1String( "CREATE TABLE bodyCounts(id integer primary key, bodyType varchar, count int )" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // add an entry for stars into bodyCounts table
    if ( !q.exec( QLatin1String( "INSERT INTO bodyCounts(bodyType, count) values('star',0)" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // create a trigger so that the star field in bodyCounts is updated every time there's a new star added to the galaxy
    if ( !q.exec( QLatin1String( "CREATE TRIGGER updateStarCountAdd INSERT ON stars BEGIN UPDATE bodyCounts SET count=count+1 WHERE bodyType = 'star'; END" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );
    if ( !q.exec( QLatin1String( "CREATE TRIGGER updateStarCountDel DELETE ON stars BEGIN UPDATE bodyCounts SET count=count-1 WHERE bodyType = 'star'; END" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // add an entry for sectors into bodyCounts table
    if ( !q.exec( QLatin1String( "INSERT INTO bodyCounts(bodyType, count) values('sector',0)" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // create a trigger so that the sector field in bodyCounts is updated every time there's a new sector added to the galaxy
    if ( !q.exec( QLatin1String( "CREATE TRIGGER updateSectorCountAdd INSERT ON sectors BEGIN UPDATE bodyCounts SET count=count+1 WHERE bodyType = 'sector'; END" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );
    if ( !q.exec( QLatin1String( "CREATE TRIGGER updateSectorCountSub DELETE ON sectors BEGIN UPDATE bodyCounts SET count=count-1 WHERE bodyType = 'sector'; END" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // add an entry for planet into bodyCounts table
    if ( !q.exec( QLatin1String( "INSERT INTO bodyCounts(bodyType, count) values('planet',0)" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );

    // create a trigger so that the planets field in bodyCounts is updated every time there's a new planet added to the galaxy
    if ( !q.exec( QLatin1String( "CREATE TRIGGER updatePlanetCountAdd INSERT ON planets BEGIN UPDATE bodyCounts SET count=count+1 WHERE bodyType = 'planet'; END" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );
    if ( !q.exec( QLatin1String( "CREATE TRIGGER updatePlanetCountSub DELETE ON planets BEGIN UPDATE bodyCounts SET count=count-1 WHERE bodyType = 'planet'; END" ) ) )
        THROW_EXCEPTION( "Could not create table in star catalogue. QSqlError: %s", q.lastError().text().toStdString().c_str() );



}



void GalaxyContainer::PrepareTransaction() {

    mStarDatabase.transaction();

}


void GalaxyContainer::GenerateStar() {

    Star s = mGalaxyGenerator->CreateStar();
    QSqlQuery q;
    q.prepare( QLatin1String( "INSERT INTO stars(name, gRadius, theta, phi, axisX, axisY, axisZ, specClass, mass, radius, luminosity, temperature, sectorName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)" ) );

    q.addBindValue( QLatin1String( s.GetName().c_str() ) );
    q.addBindValue( s.GetLocation().x );
    q.addBindValue( s.GetLocation().y );
    q.addBindValue( s.GetLocation().z );
    q.addBindValue( s.GetAxis().x );
    q.addBindValue( s.GetAxis().y );
    q.addBindValue( s.GetAxis().z );
    q.addBindValue( s.GetSpectralClass() );
    q.addBindValue( s.GetMass() );
    q.addBindValue( s.GetRadius() );
    q.addBindValue( s.GetLuminosity() );
    q.addBindValue( s.GetTemperature() );

    int r = floor( s.GetLocation().x / mRadialGranularity );
    int t = floor( s.GetLocation().y / mLongitudeGranularity );
    int p = floor( (M_PI/2-s.GetLocation().z) / mLatitudeGranularity );
    char sectorName[50];
    sprintf( sectorName, "sect%02d%02d%02d", r, t, p );
    q.addBindValue( QLatin1String( sectorName ) );

    q.exec();

    char str[100];
    sprintf( str, "SELECT * FROM sectors WHERE designation = '%s'", sectorName );
    q.exec( QLatin1String( str ) );
    if ( q.next() ) {
        sprintf( str, "UPDATE sectors SET starCount=starCount+1 WHERE designation = '%s'", sectorName );
        q.exec( QLatin1String( str ) );
    } else {
        q.prepare( QLatin1String( "INSERT INTO sectors(designation, rStart, rEnd, thetaStart, thetaEnd, phiStart, phiEnd, starCount) values(?,?,?,?,?,?,?,?)" ) );
        q.addBindValue( QLatin1String( sectorName ) );
        q.addBindValue(              r *    mRadialGranularity );
        q.addBindValue(          (r+1) *    mRadialGranularity );
        q.addBindValue(              t * mLongitudeGranularity );
        q.addBindValue(          (t+1) * mLongitudeGranularity );
        q.addBindValue( M_PI/2 -     p *  mLatitudeGranularity );
        q.addBindValue( M_PI/2 - (p+1) *  mLatitudeGranularity );
        q.addBindValue(                                      1 );
        q.exec();
    }

}


void GalaxyContainer::CommitTransaction() {

    mStarDatabase.commit();

}


void GalaxyContainer::CloseDatabase() {

    mStarDatabase.close();

}



bool GalaxyContainer::GetStarIndex( int index, Star *s ) {

    char str[100];
    sprintf( str, "SELECT * FROM stars WHERE ID = %d", index );
    QSqlQuery q;
    q.exec( QLatin1String( str ) );

    if ( q.next() ) {

        *s = Star( q.value(STAR_INDEX_NAME).toString().toStdString(),
                   Ogre::Vector3( q.value(STAR_INDEX_AXISX).toDouble(), q.value(STAR_INDEX_AXISY).toDouble(), q.value(STAR_INDEX_AXISZ).toDouble() ),
                   q.value(STAR_INDEX_MASS).toDouble(),
                   q.value(STAR_INDEX_RADIUS).toDouble(),
                   q.value(STAR_INDEX_LUM).toDouble(),
                   q.value(STAR_INDEX_TEMP).toDouble(),
                   Ogre::Vector3( q.value(STAR_INDEX_GRADIUS).toDouble(), q.value(STAR_INDEX_LONG).toDouble(), q.value(STAR_INDEX_LAT).toDouble() ),
                   q.value(STAR_INDEX_SPECCLASS).toString().toStdString().c_str() );
        return true;

    }

    return false;
}


void GalaxyContainer::GetPlanetsInSystem( Body *parentBody, std::vector<Planet> *pPlanet ) {

    char str[100];
    sprintf( str, "SELECT * FROM planets WHERE parentName = '%s'", parentBody->GetName().c_str() );
    QSqlQuery q;
    q.exec( QLatin1String( str ) );

    while( q.next() ) {

        pPlanet->push_back(
                    Planet(
                        q.value(PLANET_INDEX_NAME).toString().toStdString(),
                        Ogre::Vector3( q.value(PLANET_INDEX_AXISX).toDouble(), q.value(PLANET_INDEX_AXISY).toDouble(), q.value(PLANET_INDEX_AXISZ).toDouble() ),
                        q.value(PLANET_INDEX_MASS).toDouble(),
                        q.value(PLANET_INDEX_RADIUS).toDouble(),
                        Planet::PlanetType(q.value(PLANET_INDEX_TYPE).toInt()),
                        q.value(PLANET_INDEX_ROT_SPEED).toDouble(),
                        Orbit(
                            q.value(PLANET_INDEX_SEMIAXIS).toDouble(),
                            q.value(PLANET_INDEX_ECCENTRICITY).toDouble(),
                            q.value(PLANET_INDEX_LONGASCNODE).toDouble(),
                            q.value(PLANET_INDEX_INCLINATION).toDouble(),
                            q.value(PLANET_INDEX_ARGPERIAPSIS).toDouble(),
                            q.value(PLANET_INDEX_MASS).toDouble() + parentBody->GetMass(),
                            parentBody->GetAxis()
                            ),
                        parentBody->GetName()
                        )
                    );

    };

}



bool GalaxyContainer::AddPlanet( Planet &p, Body *pParent ) {


    QSqlQuery q;
    q.prepare( QLatin1String( "INSERT INTO planets(name, type, mass, radius, rotationSpeed, axisX, axisY, axisZ, semimajorAxis, eccentricity, longitudeAscendingNode, inclination, argumentPeriapsis, parentName) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ) );

    q.addBindValue( QString(p.GetName().c_str()) );
    q.addBindValue( p.GetType() );
    q.addBindValue( p.GetMass()  );
    q.addBindValue( p.GetRadius() );
    q.addBindValue( p.GetRotationSpeed() );
    q.addBindValue( p.GetAxis().x );
    q.addBindValue( p.GetAxis().y );
    q.addBindValue( p.GetAxis().z );
    q.addBindValue( p.GetOrbit()->GetSemimajorAxis() );
    q.addBindValue( p.GetOrbit()->GetEccentricity() );
    q.addBindValue( p.GetOrbit()->GetLongitudeOfAscendingNode() );
    q.addBindValue( p.GetOrbit()->GetInclination() );
    q.addBindValue( p.GetOrbit()->GetArgumentOfPeriapsis() );
    q.addBindValue( QString(pParent->GetName().c_str()) );


    if ( !q.exec() ) {
        QMessageBox *msg = new QMessageBox( "Could not create random planet", q.lastError().text(), QMessageBox::Critical, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton, NULL );
        msg->exec();
        return false;
    }

    return true;

}




unsigned int GalaxyContainer::GetStarCount() {

    QSqlQuery q;
    q.exec( QLatin1String( "SELECT count FROM bodyCounts WHERE bodyType = 'star'" ) );
    if ( q.next() ) {
        return q.value(0).toInt();
    }
    return 0;

}


double GalaxyContainer::GetRadialGranularity() {
    return mRadialGranularity;
}


double GalaxyContainer::GetLatitudeGranularity() {
    return mLatitudeGranularity;
}


double GalaxyContainer::GetLongitudeGranularity() {
    return mLongitudeGranularity;
}


double GalaxyContainer::GetGalacticRadius() {
    return mGalaxyGenerator->GetGalacticRadius();
}


void GalaxyContainer::RedefineSectors( double rGranularity, double lonGranularity, double latGranularity ) {

    mRadialGranularity = rGranularity;
    mLongitudeGranularity = lonGranularity;
    mLatitudeGranularity = latGranularity;

    PrepareTransaction();

    QSqlQuery q;
    // delete all rows of the sectors table
    q.exec( QLatin1String( "DELETE FROM sectors" ) );

    q.exec( QLatin1String( "SELECT name,gRadius,theta,phi FROM stars" ) );

    while ( q.next() ) {

        int r = floor(         q.value(1).toFloat()  /    mRadialGranularity );
        int t = floor(         q.value(2).toFloat()  / mLongitudeGranularity );
        int p = floor( (M_PI/2-q.value(3).toFloat()) /  mLatitudeGranularity );
        char sectorName[50];
        sprintf( sectorName, "sect%02d%02d%02d", r, t, p );

        QSqlQuery q1;

        char str[100];
        sprintf( str, "UPDATE stars SET sectorName='%s' WHERE name='%s'", sectorName, q.value(0).toString().toStdString().c_str() );
        q1.exec( sectorName );

        sprintf( str, "SELECT * FROM sectors WHERE designation = '%s'", sectorName );
        q1.exec( QLatin1String( str ) );
        if ( q1.next() ) {
            sprintf( str, "UPDATE sectors SET starCount=starCount+1 WHERE designation = '%s'", sectorName );
            q1.exec( QLatin1String( str ) );
        } else {
            q1.prepare( QLatin1String( "INSERT INTO sectors(designation, rStart, rEnd, thetaStart, thetaEnd, phiStart, phiEnd, starCount) values(?,?,?,?,?,?,?,?)" ) );
            q1.addBindValue( QLatin1String( sectorName ) );
            q1.addBindValue(              r *    mRadialGranularity );
            q1.addBindValue(          (r+1) *    mRadialGranularity );
            q1.addBindValue(              t * mLongitudeGranularity );
            q1.addBindValue(          (t+1) * mLongitudeGranularity );
            q1.addBindValue( M_PI/2 -     p *  mLatitudeGranularity );
            q1.addBindValue( M_PI/2 - (p+1) *  mLatitudeGranularity );
            q1.addBindValue(                                      1 );
            q1.exec();
        }

    };

    CommitTransaction();

}



QSqlDatabase *GalaxyContainer::GetStarDatabase() {
    return &mStarDatabase;
}


void GalaxyContainer::GetSector( int sectorId, GalaxyContainer::SectorData *pSector ) {

    char str[100];
    sprintf( str, "SELECT * FROM sectors WHERE ID = %d", sectorId );
    QSqlQuery q;
    q.exec( QLatin1String( str ) );

    if ( q.next() ) {
        pSector->mDesignation = q.value(1).toString().toStdString();
        pSector->mRadiusStart = q.value(2).toFloat();
        pSector->mRadiusEnd   = q.value(3).toFloat();
        pSector->mThetaStart  = q.value(4).toFloat();
        pSector->mThetaEnd    = q.value(5).toFloat();
        pSector->mPhiStart    = q.value(6).toFloat();
        pSector->mPhiEnd      = q.value(7).toFloat();
        pSector->mStarCount   = q.value(8).toInt();
    } else {
        pSector->mStarCount = -1;
    }

}

void GalaxyContainer::GetSector( int r, int t, int p, GalaxyContainer::SectorData *pSector ) {

    char str[100];
    sprintf( str, "SELECT * FROM sectors WHERE designation = 'sect%02d%02d%02d'", r, t, p );
    QSqlQuery q;
    q.exec( QLatin1String( str ) );

    if ( q.next() ) {
        pSector->mId          = q.value(0).toInt();
        pSector->mDesignation = q.value(1).toString().toStdString();
        pSector->mRadiusStart = q.value(2).toFloat();
        pSector->mRadiusEnd   = q.value(3).toFloat();
        pSector->mThetaStart  = q.value(4).toFloat();
        pSector->mThetaEnd    = q.value(5).toFloat();
        pSector->mPhiStart    = q.value(6).toFloat();
        pSector->mPhiEnd      = q.value(7).toFloat();
        pSector->mStarCount   = q.value(8).toInt();
    } else {
        pSector->mStarCount = -1;
    }

}



int GalaxyContainer::GetSectorId( int r, int t, int p ) {

    char str[100];
    sprintf( str, "SELECT id FROM sectors WHERE designation = 'sect%02d%02d%02d'", r, t, p );
    QSqlQuery q;
    q.exec( QLatin1String( str ) );

    if ( q.next() )
        return q.value(0).toInt();
    return -1;

}




