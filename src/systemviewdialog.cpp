#include "systemviewdialog.h"
#include "ui_systemviewdialog.h"

#include "systemview.h"

SystemViewDialog::SystemViewDialog(SystemView *pView, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SystemViewDialog),
    mSystemView(pView)
{
    ui->setupUi(this);
}

SystemViewDialog::~SystemViewDialog()
{
    delete ui;
}

void SystemViewDialog::on_randomSystemButton_pressed()
{
    mSystemView->GenerateRandomSystem();
}

void SystemViewDialog::on_simTimeStep_valueChanged(double arg1)
{
    mSystemView->SetSimulationTimeStep(arg1);
}
