#include <cfloat>
#include <Ogre.h>

#include <QFileDialog>
#include <QDesktopServices>

#include "galaxyview.h"
#include "galaxyviewcontroller.h"
#include "galaxygeneratorconfigdialog.h"
#include "ui_galaxygeneratorconfigdialog.h"
#include "galaxycontainer.h"
#include "galaxygenerator.h"


GalaxyGeneratorConfigDialog::GalaxyGeneratorConfigDialog(GalaxyView *view, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GalaxyGeneratorConfigDialog),
    mGalaxyView(view)
{
    ui->setupUi(this);
    ui->randomSeedBox->setValue(rand());
}

GalaxyGeneratorConfigDialog::~GalaxyGeneratorConfigDialog()
{
    delete ui;

}


void GalaxyGeneratorConfigDialog::on_okCancel_accepted()
{
    // Create the new galaxy
    GalaxyGenerator *pGen;

    QString fileName = QFileDialog::getSaveFileName(
                NULL,
                "Select save folder",
                QDesktopServices::storageLocation(QDesktopServices::HomeLocation),
                "Galaxy Studio File (*.galaxy)",
                NULL,
                QFileDialog::ShowDirsOnly
                );

    if ( fileName == "" )
        return;

    double maxTurns = ui->maxTurns->value() / 10.0;
    double galacticRadius = ui->galacticRadius->value();
    double galacticThickness = ui->galacticThickness->value();
    double armAlpha = ui->armAlpha->value() / 10000.0;
    double haloAlpha = ui->HaloAlpha->value() / 100.0;
    double haloRadius = ui->galacticRadius->value() * (double)ui->haloRadius->value() / 100.0;
    unsigned int globCount = ui->globCount->value();
    double globSize = ui->globSize->value();
    double globVar = ui->globVar->value();
    unsigned int radialDivisions = ui->radialDivisions->value();
    unsigned int longitudeDivisions = ui->longitudeDivisions->value();
    unsigned int latitudeDivisions = ui->latitudeDivisions->value();

    srand( ui->randomSeedBox->value() );

    pGen = new GalaxyGenerator(
            galacticRadius,
            galacticThickness,
            armAlpha,
            maxTurns,
            haloAlpha,
            haloRadius,
            globCount,
            globSize,
            globVar );

    // TODO: ADD A WIDGET TO SET THE GRANULARITY
    mGalaxyView->GetViewController()->createNewGalaxy( ui->starCount->value(), galacticRadius/radialDivisions, 2*M_PI/longitudeDivisions, M_PI/(latitudeDivisions), pGen, fileName );

}

void GalaxyGeneratorConfigDialog::on_okCancel_rejected()
{
    // Cancel the dialog
}



void GalaxyGeneratorConfigDialog::on_okCancel_apply() {

}


void GalaxyGeneratorConfigDialog::on_spiralType_currentIndexChanged(int index) {

}


void GalaxyGeneratorConfigDialog::on_maxTurns_sliderMoved(int position) {

}
