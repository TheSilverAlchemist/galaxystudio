
#include <cstdlib>
#include <climits>
#include <string>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <Ogre.h>

#include "Globals.h"




double gRandomNoise[NOISE_LENGTH];


void GenerateRandomNoise() {

    memset( gRandomNoise, 0, sizeof(double) * NOISE_LENGTH );

    int i = 0, j = 0;
    double rNums[NOISE_LENGTH+200];
    for ( i = 0; i < NOISE_LENGTH+200; i++ )
        rNums[i] = rand() / (double)UINT_MAX;

    double dMax = -1000;
    double dMean = 0;
    double d = 0;

    for ( i = 200; i < NOISE_LENGTH+200; i++ ) {

        d = 0;
        for ( j = i; j > i-200; j-- ) {

            d += rNums[j];

        }
        dMean += d;
        gRandomNoise[i-200] = d;

    }

    dMean /= NOISE_LENGTH;

    for ( i = 0; i < NOISE_LENGTH; i++ ) {
        gRandomNoise[i] -= dMean;
        if ( dMax < gRandomNoise[i] )
            dMax = gRandomNoise[i];
    }

    for ( i = 0; i < NOISE_LENGTH; i++ ) {
        gRandomNoise[i] /= dMax;
    }

}




double gaussianRandom( double m, double s ) {

    return m + s * sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX );

}




Ogre::Vector3 gaussian3D( Ogre::Vector3 m, Ogre::Vector3 s ) {

    Ogre::Vector3 p, dp;
    double f = (1/(4*M_PI*M_PI*sqrt(s.x*s.y*s.z))), d, prob;
    while ( 1 ) {
        p.x = Ogre::Math::RangeRandom(0,1);
        p.y = Ogre::Math::RangeRandom(0,1);
        p.z = Ogre::Math::RangeRandom(0,1);
        dp = (p - m)*(p - m);
        prob = f*exp( -0.5*( dp.x / s.x + dp.y / s.y + dp.z / s.z ) );
        d = rand()/(double)RAND_MAX;
        if ( d <= prob )
            break;
    };
    return p;

}




