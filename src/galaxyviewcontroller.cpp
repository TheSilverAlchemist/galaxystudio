

#include "LogManager.h"
#include "OgreWidget.h"
#include <Application.h>

#include <OgreEntity.h>
#include <OgreRenderWindow.h>
#include <OgreResourceGroupManager.h>
#include <OgreRoot.h>
#include <OgreBillboard.h>
#include <OgreBillboardSet.h>
#include "MovableText.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QSettings>
#include <QMenuBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>

#include <cfloat>

#include "Globals.h"
#include "star.h"
#include "galaxyview.h"
#include "sectorview.h"
#include "galaxycontainer.h"
#include "galaxygeneratorconfigdialog.h"
#include "maincontroldialog.h"
#include "galaxygenerator.h"
#include "systemview.h"

namespace QtOgre
{
    GalaxyViewController::GalaxyViewController(void)
        :GameLogic()
    {
    }

    void GalaxyViewController::initialise(void)
    {

        mAppLog = mApplication->createLog("Galaxy");
        Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
        mGalaxyView = new GalaxyView( this );
        mGalaxyView->initialise();
        mState = TS_Galaxy;
    }

    void GalaxyViewController::update(void)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->update();
        else if ( mState == TS_Sector )
            mSectorView->update();
        else if ( mState == TS_System )
            mSystemView->update();
    }

    void GalaxyViewController::shutdown(void)
    {
        delete mCurrentGalaxy;
        mGalaxyView->shutdown();
        delete mGalaxyView;
        if ( mSectorView ) {
            mSectorView->shutdown();
            delete mSectorView;
        }
        if ( mSystemView ) {
            mSystemView->shutdown();
            delete mSystemView;
        }
    }

    void GalaxyViewController::onKeyPress(QKeyEvent* event)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->onKeyPress(event);
        else if ( mState == TS_Sector )
            mSectorView->onKeyPress(event);
        else if ( mState == TS_System )
            mSystemView->onKeyPress(event);
    }

    void GalaxyViewController::onKeyRelease(QKeyEvent* event)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->onKeyRelease(event);
        else if ( mState == TS_Sector )
            mSectorView->onKeyRelease(event);
        else if ( mState == TS_System )
            mSystemView->onKeyRelease(event);
    }

    void GalaxyViewController::onMousePress(QMouseEvent* event)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->onMousePress(event);
        else if ( mState == TS_Sector )
            mSectorView->onMousePress(event);
        else if ( mState == TS_System )
            mSystemView->onMousePress(event);
    }

    void GalaxyViewController::onMouseRelease(QMouseEvent* event)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->onMouseRelease(event);
        else if ( mState == TS_Sector )
            mSectorView->onMouseRelease(event);
        else if ( mState == TS_System )
            mSystemView->onMouseRelease(event);
    }

    void GalaxyViewController::onMouseMove(QMouseEvent* event)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->onMouseMove(event);
        else if ( mState == TS_Sector )
            mSectorView->onMouseMove(event);
        else if ( mState == TS_System )
            mSystemView->onMouseMove(event);
    }

    void GalaxyViewController::onWheel(QWheelEvent* event)
    {
        if ( mState == TS_Galaxy )
            mGalaxyView->onWheel(event);
        else if ( mState == TS_Sector )
            mSectorView->onWheel(event);
        else if ( mState == TS_System )
            mSystemView->onWheel(event);
    }

    void GalaxyViewController::createNewGalaxy( unsigned int starCount, double rGranularity, double lonGranularity, double latGranularity, GalaxyGenerator *pGen, QString fileName ) {

        if ( mCurrentGalaxy ) {

            delete mCurrentGalaxy;
            mCurrentGalaxy = NULL;

        }

        try
        {
            mCurrentGalaxy = new GalaxyContainer( starCount, rGranularity, lonGranularity, latGranularity, pGen, fileName );
        } catch( CException &e ) {
            char strError[256];
            sprintf( strError, "Exception occurred in %s at line %d: %s", e.mFilename.c_str(), e.mLineNumber, e.mMessage.c_str() );
            QMessageBox *msg = new QMessageBox( "New Galaxy", strError, QMessageBox::Critical, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton, mApplication->mainWidget() );
            msg->exec();
            return;
        }

    }



    void GalaxyViewController::doGalaxyLoad() {

        QString fileName = QFileDialog::getExistingDirectory(
                    mApplication->mainWidget(),
                    "Select galaxy to load",
                    QDesktopServices::storageLocation(QDesktopServices::HomeLocation)
                    );

        if ( fileName == "" )
            return;

        if ( mCurrentGalaxy ) {
            delete mCurrentGalaxy;
            mCurrentGalaxy = NULL;
        }

        try
        {
            mCurrentGalaxy = new GalaxyContainer( fileName );
        } catch( CException &e ) {
            char strError[256];
            sprintf( strError, "Exception occurred in %s at line %d: %s", e.mFilename.c_str(), e.mLineNumber, e.mMessage.c_str() );
            QMessageBox *msg = new QMessageBox( "New Galaxy", strError, QMessageBox::Critical, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton, mApplication->mainWidget() );
            msg->exec();
            return;
        }

    }


    GalaxyContainer *GalaxyViewController::GetGalaxyContainer() {
        return mCurrentGalaxy;
    }

    Application *GalaxyViewController::GetApplication() {
        return mApplication;
    }


    void GalaxyViewController::GoToSector( int sectorId ) {

        mGalaxyView->halt();
        mSectorView = new SectorView( sectorId, this );
        mSectorView->initialise();
        mState = TS_Sector;

    }

    void GalaxyViewController::GoToSystem( int starId ) {
        mSectorView->halt();
        mSystemView = new SystemView( this, starId );
        mSystemView->initialise();
        mState = TS_System;
    }


    void GalaxyViewController::ReturnToGalaxy() {
        mSectorView->shutdown();
        delete mSectorView;
        mSectorView = NULL;
        mGalaxyView->resume();
        mState = TS_Galaxy;
    }


}
