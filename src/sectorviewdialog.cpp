
#include <Ogre.h>

#include <QtSql/QtSql>

#include "sectorviewdialog.h"
#include "ui_sectorviewdialog.h"

#include "star.h"
#include "sectorview.h"

#include "galaxyviewcontroller.h"

SectorViewDialog::SectorViewDialog(SectorView *pView, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SectorViewDialog),
    mSectorView(pView)
{
    ui->setupUi(this);
    mSelectedStar = NULL;
    ui->currentSectorLabel->setText(mSectorView->GetSectorDesignation().c_str());
}

SectorViewDialog::~SectorViewDialog()
{
    delete ui;
}






void SectorViewDialog::SetSelectedStarData( Star *pStar ) {

    char str[200];

    ui->designationEdit->setText( QString( pStar->GetName().c_str() ) );
    ui->classLabel->setText( QString("Spectral Class: ") + QString(pStar->GetSpectralClass()) );

    sprintf( str, "Mass: %.02f Sol", pStar->GetMass() );
    ui->massLabel->setText(  QString( str ) );

    sprintf( str, "Radius: %.02f Sol", pStar->GetRadius() );
    ui->radiusLabel->setText(  QString( str ) );

    sprintf( str, "Luminosity: %.02f Sol", pStar->GetLuminosity() );
    ui->luminosityLabel->setText(  QString( str ) );

    sprintf( str, "Temperature: %.02f Sol", pStar->GetTemperature() );
    ui->temperatureLabel->setText(  QString( str ) );

    mSelectedStar = pStar;

}





void SectorViewDialog::on_viewSystemButton_pressed()
{
    // TODO: enter the system
    if ( !mSelectedStar )
        return;
    QSqlQuery q;
    char str[200];
    sprintf(str,"SELECT id FROM stars WHERE name == '%s'",mSelectedStar->GetName().c_str());
    q.exec(str);
    if( !q.next() )
        return;
    mSectorView->GetViewController()->GoToSystem(q.value(0).toInt());
}

void SectorViewDialog::on_leaveSectorButton_pressed()
{
    // TODO: Leave the sector
    mSectorView->GetViewController()->ReturnToGalaxy();
}

void SectorViewDialog::on_designationEdit_editingFinished()
{

    if ( !mSelectedStar )
        return;

    QSqlQuery q;
    char str[200];
    std::string strNewName = ui->designationEdit->text().toStdString();

    sprintf( str, "SELECT id FROM stars WHERE name = '%s'", strNewName.c_str() );
    q.exec( QString( str ) );

    if ( q.next() ) {
        ui->designationEdit->setText( QString( mSelectedStar->GetName().c_str() ) );
        return;
    }

    sprintf( str, "SELECT id FROM stars WHERE name='%s'", mSelectedStar->GetName().c_str() );
    q.exec( QString( str ) );

    if ( !q.next() )
        return;

    int id = q.value(0).toInt();

    sprintf( str, "UPDATE stars SET name='%s' WHERE id=%d", strNewName.c_str(), id );
    q.exec( QString( str ) );

    mSelectedStar->SetName( strNewName );
    ui->designationEdit->setText( QString( strNewName.c_str() ) );

}

void SectorViewDialog::on_moveModeButton_pressed()
{
    SectorView::MovementMode m = mSectorView->GetMovementMode();
    if ( m == SectorView::MM_ArcBall ) {
        m = SectorView::MM_StarshipMode;
        ui->moveModeButton->setText("Arcball Mode");
    } else {
        m = SectorView::MM_ArcBall;
        ui->moveModeButton->setText("Starship Mode");
    }
    mSectorView->SetMovementMode(m);
}





