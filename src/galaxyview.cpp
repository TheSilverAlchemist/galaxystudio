#include "OgreWidget.h"
#include <Application.h>

#include <OgreEntity.h>
#include <OgreRenderWindow.h>
#include <OgreResourceGroupManager.h>
#include <OgreRoot.h>
#include <OgreBillboard.h>
#include <OgreBillboardSet.h>
#include <OgreManualObject.h>
#include "MovableText.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QSettings>
#include <QMenuBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>

#include <cfloat>

#include "galaxygeneratorconfigdialog.h"
#include "maincontroldialog.h"

#include "galaxycontainer.h"

#include "galaxyview.h"

#include "star.h"

GalaxyView::GalaxyView(QtOgre::GalaxyViewController *vc) : mViewController(vc)
{
}



void GalaxyView::initialise(void)
{

    // Create the generic scene manager
    mSceneManager = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC, "GalaxySceneManager");

    //Set up scene
    mCamera = mSceneManager->createCamera("camera");
    // Position it at 500 in Z direction
    mCamera->setPosition(Ogre::Vector3(500,0,0));
    // Look back along -Z
    mCamera->lookAt(Ogre::Vector3(-1,0,0));
    mCamera->setNearClipDistance(5);
    mCamera->setFarClipDistance(1000000);
    // Create one viewport, entire window
    Ogre::Viewport* vp = mViewController->GetApplication()->ogreRenderWindow()->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    mSceneManager->setAmbientLight( Ogre::ColourValue::White );

    mStarBillboardSet = mSceneManager->createBillboardSet( "galaxy", 1000 );
    mStarBillboardSet->setMaterialName("Flare");
    mGalaxySceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
    mGalaxySceneNode->attachObject( mStarBillboardSet );

    mHighlightSectorNode = NULL;
    mHighlightedSector = -1;

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio( Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));


    mSceneManager->setAmbientLight( Ogre::ColourValue( 1, 1, 1 ) );

    GenerateAstrolabe();
    GenerateInclinometer();
    mRayQuery = mSceneManager->createRayQuery( Ogre::Ray() );
    mAstrolabeMark = 0;
    mInclinometerMark = 0;

    mTime = new QTime;
    mTime->start();

    mIsFirstFrame = true;

    mGalaxyGen = new GalaxyGeneratorConfigDialog( this );
    mMainControl = new MainControlDialog(this);

    mMainControl->show();
    mAction = MA_None;

}





void GalaxyView::resume() {

    if( mViewController->GetApplication()->ogreRenderWindow()->getNumViewports() != 0)
        mViewController->GetApplication()->ogreRenderWindow()->removeAllViewports();

    Ogre::Viewport* vp = mViewController->GetApplication()->ogreRenderWindow()->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));
    mMainControl->show();

}




void GalaxyView::update(void)
{
    mLastFrameTime = mCurrentTime;
    mCurrentTime = mTime->elapsed();

    float timeElapsedInSeconds = (mCurrentTime - mLastFrameTime) / 1000.0f;

    if( mKeyStates[Qt::Key_Shift] ) {

        if ( mKeyStates[Qt::Key_F] )
            mMainControl->show();

    }

    if(!mIsFirstFrame)
    {
        QPoint mouseDelta = mCurrentMousePos - mLastFrameMousePos;
        Ogre::Vector3 pos = mCamera->getPosition();
        if ( mAction == MA_Rotate ) {
            if ( mouseDelta.x() != 0 || mouseDelta.y() != 0 ) {
                Ogre::Vector3 vec = -pos.normalisedCopy();
                Ogre::Quaternion rot1 = Ogre::Quaternion( Ogre::Degree(-mouseDelta.x() * 0.13), Ogre::Vector3::UNIT_Y ) * Ogre::Quaternion( Ogre::Degree(-mouseDelta.y() * 0.13), vec.crossProduct(Ogre::Vector3::UNIT_Y) );
                pos = rot1 * pos;
            }
        } else if ( mAction == MA_Zoom ) {

            Ogre::Real dist = pos.length();
            if ( dist >= 1 ) {
                dist += mouseDelta.y();
                if ( dist < 1 )
                    dist = 1;
                pos = pos.normalisedCopy() * dist;
            }
            mMainControl->setRadiusReading( pos.length() * 125 );

        } else if ( mAction == MA_AstrolabeMove ) {

            Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(mCurrentMousePos.x()/float(mCamera->getViewport()->getActualWidth()), mCurrentMousePos.y()/float(mCamera->getViewport()->getActualHeight()));
            std::pair< bool, Ogre::Real > intersection = mouseRay.intersects( Ogre::Plane(Ogre::Vector3::UNIT_Y,0) );
            if ( intersection.first ) {
                Ogre::Vector3 p = mouseRay * intersection.second;
                mAstrolabeMark = p.angleBetween( Ogre::Vector3::UNIT_X ).valueRadians();
                if ( p.z < 0 )
                    mAstrolabeMark = 2*M_PI-mAstrolabeMark;

            }

        } else if ( mAction == MA_InclinometerMove ) {

            Ogre::Matrix3 ax = mInclinometerSceneNode->getLocalAxes();
            Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(mCurrentMousePos.x()/float(mCamera->getViewport()->getActualWidth()), mCurrentMousePos.y()/float(mCamera->getViewport()->getActualHeight()));
            std::pair< bool, Ogre::Real > intersection = mouseRay.intersects( Ogre::Plane(ax.GetColumn(2),0) );
            if ( intersection.first ) {
                Ogre::Vector3 p = mouseRay * intersection.second;
                mInclinometerMark = p.angleBetween( ax.GetColumn(0) ).valueRadians();
                if ( fabs(mInclinometerMark) > M_PI/2 )
                    mInclinometerMark = M_PI/2;
                if ( p.y < 0 )
                    mInclinometerMark = -mInclinometerMark;


                mInclinometerMarkerSceneNode->setPosition( Ogre::Vector3(cos(mInclinometerMark),sin(mInclinometerMark),0)*1.5 );
                mInclinometerMarkerSceneNode->lookAt( Ogre::Vector3(0,0,0), Ogre::Node::TS_PARENT );
                mMainControl->setLatitudeReading( mInclinometerMark * 180 / M_PI );

            }

        }

        mInclinometerMarkerSceneNode->setPosition( Ogre::Vector3(cos(mInclinometerMark),sin(mInclinometerMark),0)*1.5 );
        mInclinometerMarkerSceneNode->lookAt( Ogre::Vector3(0,0,0), Ogre::Node::TS_PARENT );
        mMainControl->setLatitudeReading( mInclinometerMark * 180 / M_PI );

        mAstrolabeMarkerSceneNode->setPosition( Ogre::Vector3(cos(mAstrolabeMark),0,sin(mAstrolabeMark))*1.5 );
        mAstrolabeMarkerSceneNode->lookAt( Ogre::Vector3(0,0,0), Ogre::Node::TS_PARENT );
        mMainControl->setLongitudeReading( mAstrolabeMark * 180 / M_PI );



        mCamera->setPosition( pos );
        mCamera->lookAt( -pos );

        double s = pos.length() * 0.25;
        mAstrolabeSceneNode->setScale( s, s, s );
        mInclinometerSceneNode->setScale( s, s, s );
        mInclinometerSceneNode->lookAt( Ogre::Vector3(pos.x, 0, pos.z), Ogre::Node::TS_WORLD );

        if ( mKeyStates[Qt::Key_Q] ) {
            Ogre::Radian r = mCamera->getFOVy();
            if ( r < Ogre::Radian(Ogre::Math::PI) ) {
                r += Ogre::Radian( 0.01 * Ogre::Math::PI );
                mCamera->setFOVy( r );
            }
        } else if ( mKeyStates[Qt::Key_A] ) {
            Ogre::Radian r = mCamera->getFOVy();
            if ( r > Ogre::Radian(0) ) {
                r -= Ogre::Radian( 0.01 * Ogre::Math::PI );
                mCamera->setFOVy( r );
            }
        }

//            Ogre::Vector3 v = Ogre::Vector3(cos(mInclinometerMark)*sin(mAstrolabeMark), sin(mInclinometerMark),cos(mInclinometerMark)*cos(mAstrolabeMark)) * pos.length() * 0.25;
//            mSceneManager->getSceneNode("tempMarkerNode")->setPosition( v );

        GalaxyContainer *pGalaxy = mViewController->GetGalaxyContainer();
        if ( pGalaxy ) {
            // work out what sector is highlighted and change if necessary
            int currLng = (int)floor( (mAstrolabeMark) / pGalaxy->GetLongitudeGranularity() );
            int currLat = (int)floor( (M_PI/2-mInclinometerMark) / pGalaxy->GetLatitudeGranularity() );
            int currRad = (int)floor( pos.length() * 125 / pGalaxy->GetRadialGranularity() );

            HighlightSector( currRad, currLng, currLat );

        }

    }
    mLastFrameMousePos = mCurrentMousePos;
    mLastFrameWheelPos = mCurrentWheelPos;

    mIsFirstFrame = false;
}




void GalaxyView::halt(void) {

    if( mViewController->GetApplication()->ogreRenderWindow()->getNumViewports() != 0)
        mViewController->GetApplication()->ogreRenderWindow()->removeAllViewports();

    mMainControl->hide();

}






void GalaxyView::shutdown(void)
{
    mSceneManager->clearScene();
    Ogre::Root::getSingleton().destroySceneManager(mSceneManager);
    delete mGalaxyGen;
    delete mMainControl;
}

void GalaxyView::onKeyPress(QKeyEvent* event)
{
    mKeyStates[event->key()] = KS_PRESSED;
}

void GalaxyView::onKeyRelease(QKeyEvent* event)
{
    mKeyStates[event->key()] = KS_RELEASED;
}

void GalaxyView::onMousePress(QMouseEvent* event)
{

    mCurrentMousePos = event->pos();
    mLastFrameMousePos = mCurrentMousePos;
    if ( event->buttons() & Qt::LeftButton && event->buttons() & Qt::RightButton ) {
        mAction = MA_Zoom;
    } else if ( event->buttons() & Qt::LeftButton ) {

        if ( mAction == MA_None ) {
            Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(mCurrentMousePos.x()/float(mCamera->getViewport()->getActualWidth()), mCurrentMousePos.y()/float(mCamera->getViewport()->getActualHeight()));
            mRayQuery->setRay(mouseRay);
            Ogre::RaySceneQueryResult &result = mRayQuery->execute();
            Ogre::RaySceneQueryResult::iterator it = result.begin();
            for ( ; it != result.end(); it++ ) {
                if ( it->movable ) {
                    if ( it->movable == mAstrolabeMarkerSceneNode->getAttachedObject("astrolabeMarker") )
                       mAction = MA_AstrolabeMove;
                    else if ( it->movable == mInclinometerMarkerSceneNode->getAttachedObject("inclinometerMarker") )
                       mAction = MA_InclinometerMove;
                }
            }
            if ( mAction == MA_None ) {
                // Do something when empty space is clicked.
            }
        }

    } else if ( event->buttons() & Qt::RightButton ) {
        mAction = MA_Rotate;
    } else {
        mAction = MA_None;
    }

}

void GalaxyView::onMouseRelease(QMouseEvent* event)
{
    mAction = MA_None;
}

void GalaxyView::onMouseMove(QMouseEvent* event)
{
    mCurrentMousePos = event->pos();
}

void GalaxyView::onWheel(QWheelEvent* event)
{
    mCurrentWheelPos += event->delta();
}


void GalaxyView::doGalaxyCreate() {

    if ( mStarBillboardSet ) {
        mGalaxySceneNode->detachObject("galaxy");
        mSceneManager->destroyBillboardSet( "galaxy" );
        mStarBillboardSet = NULL;
    }

    mGalaxyGen->exec();

    GalaxyContainer *pGalaxy = mViewController->GetGalaxyContainer();

    unsigned int starCount = pGalaxy->GetStarCount();
    mStarBillboardSet = mSceneManager->createBillboardSet( "galaxy", starCount );

    Ogre::ColourValue c; double x, y, z;
    for ( int i = 1; i < starCount+1; i++ ) {

        Star s;
        if ( !pGalaxy->GetStarIndex(i, &s) )
            continue;
        if ( s.GetSpectralClass()[0] == 'O' )
            c = Ogre::ColourValue(    0,    0,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'B' )
            c = Ogre::ColourValue( 0.33, 0.33,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'A' )
            c = Ogre::ColourValue(  0.7,  0.7,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'F' )
            c = Ogre::ColourValue(    1,    1,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'G' )
            c = Ogre::ColourValue(    1,    1, 0.75, 1 );
        else if ( s.GetSpectralClass()[0] == 'K' )
            c = Ogre::ColourValue(    1,    1,    0, 1 );
        else if ( s.GetSpectralClass()[0] == 'M' )
            c = Ogre::ColourValue(    1,    0,    0, 1 );

        x = s.GetLocation().x * cos( s.GetLocation().z ) * cos( s.GetLocation().y );
        y = s.GetLocation().x * sin( s.GetLocation().z );
        z = s.GetLocation().x * cos( s.GetLocation().z ) * sin( s.GetLocation().y );
        mStarBillboardSet->createBillboard( x/500, y/500, z/500, c )->setDimensions(s.GetRadius(),s.GetRadius());
    }
    mStarBillboardSet->setMaterialName("Flare");
    mGalaxySceneNode->attachObject( mStarBillboardSet );


}


void GalaxyView::doGalaxyLoad() {

    if ( mStarBillboardSet ) {
        mGalaxySceneNode->detachObject("galaxy");
        mSceneManager->destroyBillboardSet( "galaxy" );
        mStarBillboardSet = NULL;
    }

    mViewController->doGalaxyLoad();
    GalaxyContainer *pGalaxy = mViewController->GetGalaxyContainer();

    unsigned int starCount = pGalaxy->GetStarCount();
    mStarBillboardSet = mSceneManager->createBillboardSet( "galaxy", starCount );

    Ogre::ColourValue c; double x, y, z;
    for ( int i = 1; i < starCount+1; i++ ) {

        Star s;
        if ( !pGalaxy->GetStarIndex(i, &s) )
            continue;
        if ( s.GetSpectralClass()[0] == 'O' )
            c = Ogre::ColourValue(    0,    0,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'B' )
            c = Ogre::ColourValue( 0.33, 0.33,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'A' )
            c = Ogre::ColourValue(  0.7,  0.7,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'F' )
            c = Ogre::ColourValue(    1,    1,    1, 1 );
        else if ( s.GetSpectralClass()[0] == 'G' )
            c = Ogre::ColourValue(    1,    1, 0.75, 1 );
        else if ( s.GetSpectralClass()[0] == 'K' )
            c = Ogre::ColourValue(    1,    1,    0, 1 );
        else if ( s.GetSpectralClass()[0] == 'M' )
            c = Ogre::ColourValue(    1,    0,    0, 1 );

        x = s.GetLocation().x * cos( s.GetLocation().z ) * cos( s.GetLocation().y );
        y = s.GetLocation().x * sin( s.GetLocation().z );
        z = s.GetLocation().x * cos( s.GetLocation().z ) * sin( s.GetLocation().y );
        mStarBillboardSet->createBillboard( x/500, y/500, z/500, c )->setDimensions(s.GetRadius(),s.GetRadius());
    }
    mStarBillboardSet->setMaterialName("Flare");
    mGalaxySceneNode->attachObject( mStarBillboardSet );

}



void GalaxyView::HighlightSector( int sectorId ) {

    GalaxyContainer::SectorData sectorData;
    mViewController->GetGalaxyContainer()->GetSector( sectorId, &sectorData );
    mAstrolabeMark = ( sectorData.mThetaStart + sectorData.mThetaEnd ) / 2;
    mInclinometerMark = ( sectorData.mPhiStart + sectorData.mPhiEnd ) / 2;
    HighlightSector( &sectorData );

}

void GalaxyView::HighlightSector( int rad, int lng, int lat ) {

    GalaxyContainer::SectorData sectorData;
    mViewController->GetGalaxyContainer()->GetSector( rad, lng, lat, &sectorData );
    HighlightSector( &sectorData );

}



void GalaxyView::HighlightSector( GalaxyContainer::SectorData *pSector ) {

    if ( pSector->mStarCount == -1 || pSector->mId == mHighlightedSector )
        return;

    mHighlightedSector = pSector->mId;

    double dTheta;
    double dPhi;
    double t, p;

    if ( pSector->mThetaStart > pSector->mThetaEnd ) {
        t = pSector->mThetaEnd;
        pSector->mThetaEnd = pSector->mThetaStart;
        pSector->mThetaStart = t;
    }

    if ( pSector->mPhiStart > pSector->mPhiEnd ) {
        t = pSector->mPhiEnd;
        pSector->mPhiEnd = pSector->mPhiStart;
        pSector->mPhiStart = t;
    }

    if ( mHighlightSectorNode ) {
        mHighlightSectorNode->detachObject("sectorHighlight");
        mSceneManager->destroyManualObject("sectorHighlight");
    } else {
        mHighlightSectorNode = mSceneManager->getRootSceneNode()->createChildSceneNode("sectorHighlightSceneNode");
    }

    Ogre::Vector3 v;

    dTheta = (pSector->mThetaEnd - pSector->mThetaStart)/8;
    dPhi = (pSector->mPhiEnd - pSector->mPhiStart)/8;

    // set up the inner square
    Ogre::ManualObject *sectorHighlight = mSceneManager->createManualObject("sectorHighlight");
    sectorHighlight->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP );

    for ( t = pSector->mThetaStart; t < pSector->mThetaEnd; t += dTheta ) {
        v = Ogre::Vector3(cos( t ) * cos( pSector->mPhiStart ), sin( pSector->mPhiStart ), sin( t ) * cos( pSector->mPhiStart ) ) * pSector->mRadiusStart / 500;
        sectorHighlight->position( v );
    }

    for ( p = pSector->mPhiStart; p < pSector->mPhiEnd; p += dPhi ) {
        v = Ogre::Vector3(cos( pSector->mThetaEnd ) * cos( p ), sin( p ), sin( pSector->mThetaEnd ) * cos( p ) ) * pSector->mRadiusStart / 500;
        sectorHighlight->position( v );
    }

    for ( t = pSector->mThetaEnd; t >= pSector->mThetaStart; t -= dTheta ) {
        v = Ogre::Vector3(cos( t ) * cos( pSector->mPhiEnd ), sin( pSector->mPhiEnd ), sin( t ) * cos( pSector->mPhiEnd ) ) * pSector->mRadiusStart / 500;
        sectorHighlight->position( v );
    }

    for ( p = pSector->mPhiEnd; p >= pSector->mPhiStart; p -= dPhi ) {
        v = Ogre::Vector3(cos( pSector->mThetaStart ) * cos( p ), sin( p ), sin( pSector->mThetaStart ) * cos( p ) ) * pSector->mRadiusStart / 500;
        sectorHighlight->position( v );
    }

    sectorHighlight->end();

    // now do the outer square
    sectorHighlight->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP );

    for ( t = pSector->mThetaStart; t < pSector->mThetaEnd; t += dTheta ) {
        v = Ogre::Vector3(cos( t ) * cos( pSector->mPhiStart ), sin( pSector->mPhiStart ), sin( t ) * cos( pSector->mPhiStart ) ) * pSector->mRadiusEnd / 500;
        sectorHighlight->position( v );
    }

    for ( p = pSector->mPhiStart; p < pSector->mPhiEnd; p += dPhi ) {
        v = Ogre::Vector3(cos( pSector->mThetaEnd ) * cos( p ), sin( p ), sin( pSector->mThetaEnd ) * cos( p ) ) * pSector->mRadiusEnd / 500;
        sectorHighlight->position( v );
    }

    for ( t = pSector->mThetaEnd; t >= pSector->mThetaStart; t -= dTheta ) {
        v = Ogre::Vector3(cos( t ) * cos( pSector->mPhiEnd ), sin( pSector->mPhiEnd ), sin( t ) * cos( pSector->mPhiEnd ) ) * pSector->mRadiusEnd / 500;
        sectorHighlight->position( v );
    }

    for ( p = pSector->mPhiEnd; p >= pSector->mPhiStart; p -= dPhi ) {
        v = Ogre::Vector3(cos( pSector->mThetaStart ) * cos( p ), sin( p ), sin( pSector->mThetaStart ) * cos( p ) ) * pSector->mRadiusEnd / 500;
        sectorHighlight->position( v );
    }

    sectorHighlight->end();


    // now do the lines connecting the outer and inner
    sectorHighlight->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

    Ogre::Vector3 points[8];
    points[0] = Ogre::Vector3(cos( pSector->mThetaStart ) * cos( pSector->mPhiStart ), sin( pSector->mPhiStart ), sin( pSector->mThetaStart ) * cos( pSector->mPhiStart ) ) * pSector->mRadiusStart / 500;
    points[1] = Ogre::Vector3(cos(   pSector->mThetaEnd ) * cos( pSector->mPhiStart ), sin( pSector->mPhiStart ), sin(   pSector->mThetaEnd ) * cos( pSector->mPhiStart ) ) * pSector->mRadiusStart / 500;
    points[2] = Ogre::Vector3(cos( pSector->mThetaStart ) * cos(   pSector->mPhiEnd ), sin(   pSector->mPhiEnd ), sin( pSector->mThetaStart ) * cos(   pSector->mPhiEnd ) ) * pSector->mRadiusStart / 500;
    points[3] = Ogre::Vector3(cos(   pSector->mThetaEnd ) * cos(   pSector->mPhiEnd ), sin(   pSector->mPhiEnd ), sin(   pSector->mThetaEnd ) * cos(   pSector->mPhiEnd ) ) * pSector->mRadiusStart / 500;
    points[4] = Ogre::Vector3(cos( pSector->mThetaStart ) * cos( pSector->mPhiStart ), sin( pSector->mPhiStart ), sin( pSector->mThetaStart ) * cos( pSector->mPhiStart ) ) *   pSector->mRadiusEnd / 500;
    points[5] = Ogre::Vector3(cos(   pSector->mThetaEnd ) * cos( pSector->mPhiStart ), sin( pSector->mPhiStart ), sin(   pSector->mThetaEnd ) * cos( pSector->mPhiStart ) ) *   pSector->mRadiusEnd / 500;
    points[6] = Ogre::Vector3(cos( pSector->mThetaStart ) * cos(   pSector->mPhiEnd ), sin(   pSector->mPhiEnd ), sin( pSector->mThetaStart ) * cos(   pSector->mPhiEnd ) ) *   pSector->mRadiusEnd / 500;
    points[7] = Ogre::Vector3(cos(   pSector->mThetaEnd ) * cos(   pSector->mPhiEnd ), sin(   pSector->mPhiEnd ), sin(   pSector->mThetaEnd ) * cos(   pSector->mPhiEnd ) ) *   pSector->mRadiusEnd / 500;

    sectorHighlight->position(points[0]);
    sectorHighlight->position(points[4]);

    sectorHighlight->position(points[1]);
    sectorHighlight->position(points[5]);

    sectorHighlight->position(points[2]);
    sectorHighlight->position(points[6]);

    sectorHighlight->position(points[3]);
    sectorHighlight->position(points[7]);

    sectorHighlight->end();
    mHighlightSectorNode->attachObject( sectorHighlight );

    mMainControl->setSectorName( QString(pSector->mDesignation.c_str()) );

}



void GalaxyView::GenerateAstrolabe() {

    mAstrolabeSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode( "astrolabeNode" );

    Ogre::ManualObject *pAstrolabe = mSceneManager->createManualObject("astrolabe");
    mAstrolabeSceneNode->attachObject( pAstrolabe );

    pAstrolabe->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP );

    for ( double t = 0; t < 2*M_PI; t+=M_PI/50 )
        pAstrolabe->position( Ogre::Vector3(cos(t),0,sin(t)) );
//        for ( int i = 0; i < 100; i++ )
//            pAstrolabe->index(i);
//        pAstrolabe->index(0);

    pAstrolabe->end();

    pAstrolabe->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

    for ( int i = 0; i < 360; i+=20 ) {
        Ogre::Vector3 v = Ogre::Vector3( cos(i*M_PI/180), 0, sin(i*M_PI/180) );
        pAstrolabe->position( v );
        pAstrolabe->position( v * 1.2 );
        char str[20];
        if ( i == 0 )
            sprintf( str, "0/360" );
        else
            sprintf( str, "%d", i );
        Ogre::MovableText *pText = new Ogre::MovableText( std::string("astrolabeDegree") + std::string(str), str );
        Ogre::SceneNode *n = mAstrolabeSceneNode->createChildSceneNode( std::string("astrolabeDegree") + std::string(str) + std::string("SceneNode"), v * 1.25 );
        n->attachObject( pText );
        n->scale(0.05,0.05,0.05);
    }

    pAstrolabe->end();

    mAstrolabeMarkerSceneNode = mAstrolabeSceneNode->createChildSceneNode("astrolabeMarkerSceneNode", Ogre::Vector3(0,0,1.5));
    Ogre::ManualObject *pMarker = mSceneManager->createManualObject("astrolabeMarker");
    mAstrolabeMarkerSceneNode->attachObject( pMarker );
    mAstrolabeMarkerSceneNode->scale(0.1,0.1,0.1);
    mAstrolabeMarkerSceneNode->lookAt( Ogre::Vector3(0,0,0), Ogre::Node::TS_PARENT );

    pMarker->begin( "MarkerMaterial", Ogre::RenderOperation::OT_TRIANGLE_FAN );

    pMarker->position(0,0,0);
    for ( double t = 0; t < 2*M_PI; t+=M_PI/50 )
        pMarker->position( Ogre::Vector3(cos(2*M_PI-t),sin(2*M_PI-t),2) );

    pMarker->end();

    pMarker->begin( "MarkerMaterial", Ogre::RenderOperation::OT_TRIANGLE_FAN );

    pMarker->position(0,0,2);
    for ( double t = 0; t < 2*M_PI; t+=M_PI/50 )
        pMarker->position( Ogre::Vector3(cos(t),sin(t),2) );

    pMarker->end();

}

void GalaxyView::GenerateInclinometer() {

    mInclinometerSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode( "inclinometerNode" );

    Ogre::ManualObject *pInclinometer = mSceneManager->createManualObject("inclinometer");
    mInclinometerSceneNode->attachObject( pInclinometer );

    pInclinometer->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP );

    for ( double t = 0.5*M_PI; t >= -26*M_PI/50; t-=M_PI/50 )
        pInclinometer->position( Ogre::Vector3(cos(t),sin(t),0) );
//        for ( int i = 0; i < 51; i++ )
//            pInclinometer->index(i);

    pInclinometer->end();

    pInclinometer->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

    for ( int i = 90; i >= -90; i-=15 ) {
        if ( i == 0 )
            continue;
        Ogre::Vector3 v = Ogre::Vector3( cos(i*M_PI/180), sin(i*M_PI/180), 0 );
        pInclinometer->position( v );
        pInclinometer->position( v * 1.2 );
        char str[20];
        sprintf( str, "%d", i );
        Ogre::MovableText *pText = new Ogre::MovableText( std::string("inclinometerDegree") + std::string(str), str );
        Ogre::SceneNode *n = mInclinometerSceneNode->createChildSceneNode( std::string("inclinometerDegree") + std::string(str) + std::string("SceneNode"), v * 1.25 );
        n->attachObject( pText );
        n->scale(0.05,0.05,0.05);
    }

    pInclinometer->end();

    mInclinometerMarkerSceneNode = mInclinometerSceneNode->createChildSceneNode("inclinometerMarkerSceneNode", Ogre::Vector3(1.5,0,0));
    Ogre::ManualObject *pMarker = mSceneManager->createManualObject("inclinometerMarker");
    mInclinometerMarkerSceneNode->attachObject( pMarker );
    mInclinometerMarkerSceneNode->scale(0.1,0.1,0.1);
    mInclinometerMarkerSceneNode->lookAt( Ogre::Vector3(0,0,0), Ogre::Node::TS_PARENT );

    pMarker->begin( "MarkerMaterial", Ogre::RenderOperation::OT_TRIANGLE_FAN );

    pMarker->position(0,0,0);
    for ( double t = 0; t < 2*M_PI; t+=M_PI/50 )
        pMarker->position( Ogre::Vector3(cos(2*M_PI-t),sin(2*M_PI-t),2) );

    pMarker->end();

    pMarker->begin( "MarkerMaterial", Ogre::RenderOperation::OT_TRIANGLE_FAN );

    pMarker->position(0,0,2);
    for ( double t = 0; t < 2*M_PI; t+=M_PI/50 )
        pMarker->position( Ogre::Vector3(cos(t),sin(t),2) );

    pMarker->end();


}


QtOgre::GalaxyViewController *GalaxyView::GetViewController() {

    return mViewController;

}



void GalaxyView::GoToHighlightedSector() {

    mViewController->GoToSector( mHighlightedSector );

}


