
#include <fstream>

#include <Ogre.h>

#include <QPixmap>
#include <QPainter>

#include <cfloat>

#include "Globals.h"
#include "spiral.h"
#include "probabilitymap.h"



void ProbabilityMap::CalculateSpiral() {

    mSpiralArms.reserve( mNumberOfArms*10 );
    SpiralArm a;
    a.mStartPoint = Ogre::Vector2(0,0);
    a.mEnergy = a.mInitialEnergy = mInitialSpiralEnergy;
    a.mLength = 0;

    double incr = 2 * Ogre::Math::PI / mNumberOfArms;
    for ( unsigned int i = 0; i < mNumberOfArms; i++ ) {
        a.mStartPhase = i * incr;
        a.mEnergy = a.mInitialEnergy = mInitialSpiralEnergy * 0.125 * ( 1 + 7 * Ogre::Math::RangeRandom(0,1) );
        mSpiralArms.push_back( a );
    }

    bool done = false;
    unsigned int index, count;

    incr = mInitialSpiralEnergy / 200;
    mMaximumRadius = 0;

    while( !done ) {

        done = true;
        count = mSpiralArms.size();
        for ( index = 0; index < count; index++ ) {

            if ( mSpiralArms[index].mEnergy == 0 ) {
                mMaximumRadius = std::max( mMaximumRadius, mSpiral->toRadius( mSpiralArms[index].mLength ) );
                continue;
            }

            done = false;
            mSpiralArms[index].mEnergy -= incr;
            mSpiralArms[index].mLength += incr;

            if ( mSpiralArms[index].mEnergy <= 0 ) {

                mSpiralArms[index].mEnergy = 0;

//            } else {

//                if ( Ogre::Math::RangeRandom(0,1) < 0.5 ) {

//                    a.mEnergy = a.mInitialEnergy = mSpiralArms[index].mEnergy * std::min( 1.0, fabs( gaussianRandom(0,0.25) ) );
//                    if ( a.mEnergy > 0 ) {

//                        mSpiralArms[index].mEnergy -= a.mEnergy;
//                        a.mLength = 0;
//                        a.mStartPhase = mSpiralArms[index].mLength + mSpiralArms[index].mStartPhase;
//                        double r = mSpiral->toRadius( mSpiralArms[index].mLength );
//                        a.mStartPoint = Ogre::Vector2( r * cos(mSpiralArms[index].mLength), fabs(r) * sin(mSpiralArms[index].mLength) );
//                        mSpiralArms.push_back(a);

//                    }

//                }

            }

        }

    }

    mRadiusCoefficient = PROBABILITY_MAP_WIDTH / ( 2 * mMaximumRadius );
    mNumberOfArms = mSpiralArms.size();

}



#define GaussianWindow(x,y,w) exp(-0.5 * ( pow( x - w/2, 2 ) + pow( y - w/2, 2 ) ) / pow(w/5,2) ) / ( 2 * M_PI * pow(w/5,2) )



void ProbabilityMap::CalculateMap() {

    QImage image(PROBABILITY_MAP_WIDTH, PROBABILITY_MAP_WIDTH, QImage::Format_ARGB32);
    QPainter painter;

    image.fill(Qt::black);
    painter.begin(&image);

    std::vector<SpiralArm>::iterator it = mSpiralArms.begin();

    Ogre::Vector2 offset = Ogre::Vector2(PROBABILITY_MAP_WIDTH, PROBABILITY_MAP_WIDTH)/2;

    QPen pen;

    pen.setStyle( Qt::SolidLine );
    pen.setColor( Qt::white );
    pen.setCapStyle( Qt::RoundCap );
    pen.setJoinStyle( Qt::RoundJoin );
    painter.setPen( pen );


    for ( ; it != mSpiralArms.end(); it++ ) {

        double incr = it->mLength / 200;
        double initialThickness = mArmThickness * it->mInitialEnergy / mInitialSpiralEnergy;
        Ogre::Vector2 lastPoint = it->mStartPoint * mRadiusCoefficient + offset, p;
        for ( double theta = incr; theta < it->mLength; theta += incr ) {

            double r = mSpiral->toRadius( theta );
            p = ( it->mStartPoint + Ogre::Vector2( r * cos( theta + it->mStartPhase ), fabs(r) * sin( theta + it->mStartPhase ) ) ) * mRadiusCoefficient + offset;
            pen.setWidthF( initialThickness * ( 1 - theta / it->mLength ) );
            painter.setPen( pen );
            painter.drawLine( (int)lastPoint.x, (int)lastPoint.y, (int)p.x, (int)p.y );
            lastPoint = p;

        }

    }

    painter.end();

    unsigned int i, j, x, y, x1, y1, x2, y2;
    double minimum = DBL_MAX, maximum = 0;
    for ( i = 0; i < PROBABILITY_MAP_WIDTH; i++ ) {

        for ( j = 0; j < PROBABILITY_MAP_WIDTH; j++ ) {

            x1 = MAX(       i - mFilterWindowSize / 2,                     0 );
            y1 = MAX(       j - mFilterWindowSize / 2,                     0 );
            x2 = MIN( i + ceil(mFilterWindowSize / 2), PROBABILITY_MAP_WIDTH );
            y2 = MIN( j + ceil(mFilterWindowSize / 2), PROBABILITY_MAP_WIDTH );

            mProbMap[i][j] = 0;
            for ( x = x1; x < x2; x++ ) {

                for ( y = y1; y < y2; y++ ) {

                    mProbMap[i][j] += (double)qGray( image.pixel(x,y) ) / 255.0;

                }

            }

            minimum = std::min( minimum, mProbMap[i][j] );
            maximum = std::max( maximum, mProbMap[i][j] );

        }

    }

    double diff = maximum - minimum;
    for ( i = 0; i < PROBABILITY_MAP_WIDTH; i++ )
        for ( j = 0; j < PROBABILITY_MAP_WIDTH; j++ )
            mProbMap[i][j] = ( mProbMap[i][j] - minimum ) / diff;

}



ProbabilityMap::ProbabilityMap( Spiral *pSpiral, unsigned int nArms, double spiralEnergy, unsigned int windowSize, double armSize ) : mSpiral(pSpiral), mNumberOfArms(nArms), mInitialSpiralEnergy(spiralEnergy), mFilterWindowSize(windowSize), mArmThickness(armSize) {

    CalculateSpiral();
    CalculateMap();

}


ProbabilityMap::ProbabilityMap( std::ifstream &fin ) {

    std::string type;
    double t, r;
    fin >> type
        >> t
        >> r
        >> mNumberOfArms
        >> mInitialSpiralEnergy
        >> mFilterWindowSize
        >> mMaximumRadius
        >> mArmThickness;

    mSpiral = SpiralFactory::CreateSpiralFunction( type, t, r );

    SpiralArm a;
    a.mEnergy = 0;
    for ( unsigned int i = 0; i < mNumberOfArms; i++ ) {

        fin >> a.mStartPoint.x
            >> a.mStartPoint.y
            >> a.mStartPhase
            >> a.mLength
            >> a.mInitialEnergy;
        mSpiralArms.push_back( a );

    }

    CalculateMap();

}


ProbabilityMap::~ProbabilityMap() {

    mSpiralArms.clear();

}


void ProbabilityMap::saveToFile( std::ofstream &fout ) {

    fout << mSpiral->getName() << " "
         << mSpiral->getMaximumTurns() << " "
         << mSpiral->getMaximumRadius() << " "
         << mNumberOfArms << " "
         << mInitialSpiralEnergy << " "
         << mFilterWindowSize << " "
         << mMaximumRadius << " "
         << mArmThickness << "\n";

    for ( std::vector<SpiralArm>::iterator it = mSpiralArms.begin(); it != mSpiralArms.end(); it++ ) {

        fout << it->mStartPoint.x << " "
             << it->mStartPoint.y << " "
             << it->mStartPhase << " "
             << it->mLength << " "
             << it->mEnergy << " "
             << it->mInitialEnergy << "\n";

    }

}



double ProbabilityMap::GetProbability( unsigned int i, unsigned int j ) {

    return mProbMap[i][j];

}





