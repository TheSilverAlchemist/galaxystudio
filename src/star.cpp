
#include <Ogre.h>
#include <string>
#include <climits>
#include <cfloat>
#include <cmath>
#include <stdlib.h>

#include "Globals.h"
#include "star.h"


Body::Body() {}
Body::Body( std::string name, Ogre::Vector3 axis, double mass, double radius ) : mName(name), mAxis(axis), mMass(mass), mRadius(radius) {}
Body::~Body() { }



std::string Body::GetName() { return mName; }

double Body::GetMass() { return mMass; }

double Body::GetRadius() { return mRadius; }

Ogre::Vector3 Body::GetAxis() { return mAxis; }

void Body::SetName( std::string strName ) { mName = strName; }



const double Star::mStarClassDistribution[8] = { 0, 0.7653749513026881, 0.8865134644567619, 0.9626004644543621, 0.9926900035118061, 0.9986968719326693, 0.9999991000496773, 1.0 };
const double Star::mStarRadiusDistribution[8] = { 0.4, 0.7, 0.96, 1.15, 1.4, 1.8, 6.6, 100 };
const double Star::mStarMassDistribution[8] = { 0.4, 0.45, 0.8, 1.04, 1.4, 2.1, 16, 100 };
const char Star::mStarClasses[7] = { 'M', 'K', 'G', 'F', 'A', 'B', 'O' };
const char *Star::mStarSubclasses[] = { "I", "II", "III", "IV", "V" };





Star::Star() { }

Star::Star( Ogre::Vector3 pos, double prob ) {

    mLocation.x = pos.length();
    mLocation.y = atan2( pos.z, pos.x );
    mLocation.z = asin( pos.y / mLocation.x );

    if ( mLocation.y < 0 )
        mLocation.y += M_PI*2;

    //mLocation = pos;
    mAxis = Ogre::Vector3( sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX ), 1, sqrt( - 2 * log( rand()/(double)RAND_MAX ) ) * cos( 2 * M_PI * rand()/(double)RAND_MAX ) ).normalisedCopy();

    double n = prob * rand() / (double)RAND_MAX, diff = 0;
    unsigned int I = floor(8*n);
    if ( I == 8 )
        I = 7;
    diff = 8*n - I;
    if ( diff > 1 )
        diff = 0.5;

    mRadius = mStarRadiusDistribution[I] + (mStarRadiusDistribution[I+1]-mStarRadiusDistribution[I]) * diff;
    mMass = mStarMassDistribution[I] + (mStarMassDistribution[I+1]-mStarMassDistribution[I]) * diff;
    mLuminosity = pow( mMass, n*6 );
    mTemperature = pow( mLuminosity / ( mRadius * mRadius ), 0.25 );
    mSpectralClass[0] = mStarClasses[I];
    mSpectralClass[1] = (char)floor( 10 * diff ) + '0';
    mSpectralClass[2] = '\0';
    strcat( mSpectralClass, mStarSubclasses[ (int)floor( 5 * diff ) ] );
    char s[20];
    sprintf( s, "%s%4d", mSpectralClass, rand() );
    mName = std::string( s );

}


Star::Star( std::string name, Ogre::Vector3 axis, double mass, double radius, double luminosity, double temperature, Ogre::Vector3 location, const char *spectralClass )
    : Body(name, axis, mass, radius), mLuminosity(luminosity), mTemperature(temperature), mLocation(location) {

    strcpy( mSpectralClass, spectralClass );

}


Star::~Star() {  }


double Star::GetLuminosity() { return mLuminosity; }

double Star::GetTemperature() { return mTemperature; }

Ogre::Vector3 Star::GetLocation() { return mLocation; }

char *Star::GetSpectralClass() { return mSpectralClass; }





Orbit::Orbit() {}


Orbit::Orbit( double semimajorAxis, double e, double longAscNode, double i, double argPeriapsis, double bodyMass, Ogre::Vector3 centralBodyAxis )
    : mSemimajorAxis(semimajorAxis), mEccentricity(e), mLongitudeOfAscendingNode(longAscNode), mInclination(i), mArgumentOfPeriapsis(argPeriapsis) {

    Ogre::Vector3 tempVec = centralBodyAxis.crossProduct( Ogre::Vector3::UNIT_Y );
    Ogre::Matrix3 refCoord;
    refCoord.SetColumn( 0, tempVec );
    refCoord.SetColumn( 1, centralBodyAxis );
    refCoord.SetColumn( 2, centralBodyAxis.crossProduct( tempVec ) );

    mAscendingNode = Ogre::Quaternion( Ogre::Radian(mLongitudeOfAscendingNode), refCoord.GetColumn(1) ) * refCoord.GetColumn(0);
    Ogre::Vector3 coordY = Ogre::Quaternion( Ogre::Radian(mInclination), mAscendingNode ) * refCoord.GetColumn(1);
    Ogre::Vector3 coordX = Ogre::Quaternion( Ogre::Radian(mArgumentOfPeriapsis), coordY ) * mAscendingNode;
    Ogre::Vector3 coordZ = coordY.crossProduct( coordX );

    mCoordinateSystem.SetColumn( 0, coordX );
    mCoordinateSystem.SetColumn( 1, coordY );
    mCoordinateSystem.SetColumn( 2, coordZ );

    mOrbitalVelocity = sqrt( GRAVITATIONAL_CONSTANT * bodyMass / ( mSemimajorAxis*mSemimajorAxis*mSemimajorAxis ) );
    mOrbitalPeriod = 2 * M_PI / mOrbitalVelocity;

    double a = mAscendingNode.dotProduct( coordZ );
    double b = mAscendingNode.dotProduct( coordX );
    double s = sqrt( a*a + b*b );

    mSolsticeTimes[0] = asin( -mEccentricity * a / s ) - acos( - b / s );
    mSolsticeTimes[1] = mSolsticeTimes[0] + M_PI;

    mEquinoxTimes[0] = atan( -b/a );
    mEquinoxTimes[1] = mEquinoxTimes[0] + M_PI;

    mSolsticeTimes[0] /= mOrbitalVelocity;
    mSolsticeTimes[1] /= mOrbitalVelocity;
    mEquinoxTimes[0]  /= mOrbitalVelocity;
    mEquinoxTimes[1]  /= mOrbitalVelocity;

    mPeriapsis = ( 1 - mEccentricity ) * mSemimajorAxis;
    mApoapsis = ( 1 + mEccentricity ) * mSemimajorAxis;

    mCurrentTime = 0;

}


Orbit::~Orbit() { }



Ogre::Vector3 Orbit::CalculatePosition( double elapsedTime ) {

    mCurrentTime += elapsedTime;
    double theta = mCurrentTime * mOrbitalVelocity;
    double r = mSemimajorAxis * ( 1 - mEccentricity*mEccentricity ) / ( 1 + mEccentricity * cos(theta) );
    return ( mCoordinateSystem.GetColumn(0) * cos(theta) + mCoordinateSystem.GetColumn(2) * sin(theta) ) * r;

}


void Orbit::CalculateOrbitPoints( unsigned int nPoints, std::vector<Ogre::Vector3> *pPoints ) {

    for ( unsigned int i = 0; i < nPoints; i++ ) {
        double theta = i * 2 * M_PI / nPoints;
        double r = mSemimajorAxis * ( 1 - mEccentricity*mEccentricity ) / ( 1 + mEccentricity * cos(theta) );
        pPoints->push_back( ( mCoordinateSystem.GetColumn(0) * cos(theta) + mCoordinateSystem.GetColumn(2) * sin(theta) ) * r );
    }

}



// orbital parameters
double Orbit::GetSemimajorAxis() { return mSemimajorAxis; }


double Orbit::GetEccentricity() { return mEccentricity; }


double Orbit::GetLongitudeOfAscendingNode() { return mLongitudeOfAscendingNode; }


double Orbit::GetInclination() { return mInclination; }


double Orbit::GetArgumentOfPeriapsis() { return mArgumentOfPeriapsis; }

// the ascending node
Ogre::Vector3 Orbit::GetAscendingNode() { return mAscendingNode; }

// orbital coordinate system
Ogre::Matrix3 Orbit::GetCoordinateSystem() { return mCoordinateSystem; }

// Orbital period (in seconds)
double Orbit::GetOrbitalPeriod() { return mOrbitalPeriod; }

// Orbital veloctiy (in rad/sec)
double Orbit::GetOrbitalVelocity() { return mOrbitalVelocity; }

// Solstice times
double Orbit::GetFirstSolsticeTime() { return mSolsticeTimes[0]; }
double Orbit::GetSecondSolsticeTime() { return mSolsticeTimes[1]; }

// Equinox times
double Orbit::GetFirstEquinoxTime() { return mEquinoxTimes[0]; }
double Orbit::GetSecondEquinoxTime() { return mEquinoxTimes[1]; }

// periapsis and apoapsis
double Orbit::GetPeriapsis() { return mPeriapsis; }
double Orbit::GetApoapsis() { return mApoapsis; }





Planet::Planet() { }


Planet::Planet( std::string strName, Ogre::Vector3 axis, double mass, double radius, PlanetType type, double rotationSpeed, Orbit o, std::string parentName )
    : Body(strName,axis,mass,radius), mType(type), mRotationSpeed(rotationSpeed), mOrbit(o), mParentName(parentName) {

}



Planet::~Planet() {  }




Planet::PlanetType Planet::GetType() { return mType; }
double Planet::GetRotationSpeed() { return mRotationSpeed; }
Orbit *Planet::GetOrbit() { return &mOrbit; }
std::string& Planet::GetParentName() { return mParentName; }


