
#include <Ogre.h>

#include "star.h"
#include "galaxyview.h"
#include "sectorview.h"

#include "galaxycontainer.h"
#include "maincontroldialog.h"
#include "ui_maincontroldialog.h"

#include "sectorredefinedialog.h"

MainControlDialog::MainControlDialog( GalaxyView *pView, QWidget *parent) :
    mGalaxyView(pView),
    QDialog(parent),
    ui(new Ui::mainControlDialog),
    mSectorDataModel(NULL)
{
    ui->setupUi(this);
}

MainControlDialog::~MainControlDialog()
{
    delete ui;
    delete mSectorDataModel;
}


void MainControlDialog::setRadiusReading(double r) {

    char str[50];
    sprintf( str, "Radius: %.2f", r );
    ui->radiusReading->setText( str );

}


void MainControlDialog::setLatitudeReading(double lat) {

    char str[50];
    sprintf( str, "Latitude: %.2f", lat );
    ui->latitudeReading->setText( str );

}



void MainControlDialog::setLongitudeReading(double lng) {

    char str[50];
    sprintf( str, "Longitude: %.2f", lng );
    ui->longitudeReading->setText( str );

}



void MainControlDialog::setSectorName(QString n) {

    ui->sectorName->setText( QString("Sector: ") + n );

}



void MainControlDialog::on_newGalaxy_clicked()
{
    mGalaxyView->doGalaxyCreate();

    delete mSectorDataModel;
    mSectorDataModel = new QSqlTableModel();

    mSectorDataModel->setTable("sectors");
    mSectorDataModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    mSectorDataModel->select();

    mSectorDataModel->setHeaderData( 1, Qt::Horizontal, QObject::tr("Code") );
    mSectorDataModel->setHeaderData( 8, Qt::Horizontal, QObject::tr("Star Count") );

    ui->sectorTable->setModel(mSectorDataModel);
    ui->sectorTable->setEditTriggers( QAbstractItemView::NoEditTriggers );

    ui->sectorTable->hideColumn(0);
    ui->sectorTable->hideColumn(2);
    ui->sectorTable->hideColumn(3);
    ui->sectorTable->hideColumn(4);
    ui->sectorTable->hideColumn(5);
    ui->sectorTable->hideColumn(6);
    ui->sectorTable->hideColumn(7);

}

void MainControlDialog::on_openGalaxy_clicked()
{
    mGalaxyView->doGalaxyLoad();

    delete mSectorDataModel;
    mSectorDataModel = new QSqlTableModel();

    mSectorDataModel->setTable("sectors");
    mSectorDataModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    mSectorDataModel->select();

    mSectorDataModel->setHeaderData( 1, Qt::Horizontal, QObject::tr("Code") );
    mSectorDataModel->setHeaderData( 8, Qt::Horizontal, QObject::tr("Star Count") );

    ui->sectorTable->setModel(mSectorDataModel);
    ui->sectorTable->setEditTriggers( QAbstractItemView::NoEditTriggers );

    ui->sectorTable->hideColumn(0);
    ui->sectorTable->hideColumn(2);
    ui->sectorTable->hideColumn(3);
    ui->sectorTable->hideColumn(4);
    ui->sectorTable->hideColumn(5);
    ui->sectorTable->hideColumn(6);
    ui->sectorTable->hideColumn(7);

}

void MainControlDialog::on_sectorTable_clicked(const QModelIndex &index)
{
    mGalaxyView->HighlightSector( index.row() );
}

void MainControlDialog::on_redefineSectorButton_pressed()
{
    SectorRedefineDialog *pDlg = new SectorRedefineDialog( mGalaxyView );
    pDlg->show();
}

void MainControlDialog::on_enterSectorButton_pressed()
{
    mGalaxyView->GoToHighlightedSector();
}
