#include "galaxycontainer.h"
#include "galaxyviewcontroller.h"
#include "galaxyview.h"

#include "sectorredefinedialog.h"
#include "ui_sectorredefinedialog.h"


SectorRedefineDialog::SectorRedefineDialog(GalaxyView *pView, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SectorRedefineDialog),
    mGalaxyView(pView)
{
    ui->setupUi(this);
}

SectorRedefineDialog::~SectorRedefineDialog()
{
    delete ui;
}

void SectorRedefineDialog::on_okCancel_accepted()
{
    GalaxyContainer *g = mGalaxyView->GetViewController()->GetGalaxyContainer();
    unsigned int radialDivisions = ui->radialDivisions->value();
    unsigned int longitudeDivisions = ui->longitudeDivisions->value();
    unsigned int latitudeDivisions = ui->latitudeDivisions->value();
    mGalaxyView->GetViewController()->GetGalaxyContainer()->RedefineSectors( g->GetGalacticRadius()/radialDivisions, 2*M_PI/longitudeDivisions, M_PI/latitudeDivisions );
}
