#include <Application.h>
#include "galaxyviewcontroller.h"

#include <QPushButton>
#include <QIcon>

using namespace QtOgre;

int main(int argc, char *argv[])
{
    Application app(argc, argv, new GalaxyViewController);
    return app.exec(QtOgre::DisplaySettingsDialog);
}
